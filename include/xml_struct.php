<?php
$count_fields=0;
class XmlInputForm {
	var $xml_structure; // xml structure definition (array)
	var $xml_instances; // xml instances (recursive array)
	var $form = ""; // form for xml input
	var $debugToFile = 1; // toggle this to 1 to have lots of extra info on the screen
	var $mode = 1; // object access mode = 1 is edit mode, mode = 0 is view mode
	
	var $classificator_name = ""; // for temporary storage of current classificator name for recurse functions
	var $classificator_entry_id = ""; // for temporary storage of current classificator entry id

	function XmlInputForm($xml_db) {
		if(is_array($xml_db)) { 
			$this->initXmlStructureFromDatabase($xml_db);
		} else {
			// echo "No XML database found.";
		}
	}

	function test() {
		header("Content-type: text/plain");
		header("Cache-Control: no-store, no-cache");
		unset($_SESSION['xml_instances']);
		$db = dbc();
		$rs = $db->Execute("select * from meta1");
		$xml_db = $rs->GetArray();
		$this->initXmlStructureFromDatabase($xml_db);
		// $this->debug = 1;
		// $this->initXmlInstancesIntoSession();
		// $this->initXmlInstances();
		// echo "Structure:\n";
		// print_r($this->xml_structure);
		// echo "Instances:\n";
		print_r($this->xml_instances);
		echo "Form:\n";
		$this->generateXmlForm("lom/general");
		echo $this->form;		
		echo "Session:\n";
		var_export($_SESSION['xml_instances']);
	}
	
	function debug() {
		header("Content-type: text/plain");
		header("Cache-Control: no-store, no-cache");
		$this->generateXmlForm("lom/general");
		echo $this->form;
	}

	function debugToFile() {
		$handle = fopen("/tmp/lomdebug.txt", "w");
		fwrite($handle,"\nRequest:\n");
		fwrite($handle, var_export($_REQUEST,true));
		fwrite($handle,"\nSession\n");
		fwrite($handle, var_export($_SESSION,true));
		fwrite($handle,"\nCookies\n");
		fwrite($handle, var_export($_COOKIE,true));
		fclose($handle);		
	}
	
	///
	// Function displayXmlStruct($xpath) displays selected xml structure item(s).
	// Example: displayXmlStruct("lom/general");

	function initXmlStructureFromDatabase($xml_db) {
		for($i=0;$i<count($xml_db); $i++) {
			$this->xml_structure[$xml_db[$i]['xpath']]=array (
					'id' => $xml_db[$i]['id'],
					'name_lt' => $xml_db[$i]['name_lt'],
					'name_en' => $xml_db[$i]['name_en'],
					'xpath' => $xml_db[$i]['xpath'],
					'xpath2' => $xml_db[$i]['xpath2'],
					'limits' => $xml_db[$i]['limits'],
					'definition_lt' => $xml_db[$i]['definition_lt'],
					'value_lt' => $xml_db[$i]['value_lt'],
					'value_en' => $xml_db[$i]['value_en'],
					'definition2_lt' => $xml_db[$i]['definition2_lt'],
					'note_lt' => $xml_db[$i]['note_lt'],
					'sample_lt' => $xml_db[$i]['sample_lt'],
					'level' => $xml_db[$i]['level'],
					'html_type' => $xml_db[$i]['html_type'],
					'hidden' =>$xml_db[$i]['hidden'],
					'default_values' =>$xml_db[$i]['default_values']
					);									
		}
	}
	
	function initXmlStructureTest() {
		// load from mysql into $xml_structure
		$this->xml_structure = array ( 
		'lom' => array (
    					'id' => '0',
					'name_lt' => 'Mokymo(si) objektų metaduomenys',
					'name_en' => 'LOM',
					'xpath' => 'lom',
					'limits' => '1',
					'definition_lt' => 'LOM visi metaduomenys',
					'value_lt' => '',
					'value_en' => '',
					'definition2_lt' => '',
					'note_lt' => '',
					'sample_lt' => '',
					'level' => '',
					'html_type' => 'GROUP'
					),
		'lom/general' => array (
    					'id' => '1',
					'name_lt' => 'Bendroji dalis',
					'name_en' => 'General',
					'xpath' => 'lom/general',
					'limits' => '1',
					'definition_lt' => 'Ši grupė apima bendriausią informaciją apie MO',
					'value_lt' => '',
					'value_en' => '',
					'definition2_lt' => '',
					'note_lt' => '',
					'sample_lt' => '',
					'level' => '',
					'html_type' => 'GROUP'
					),
		'lom/general/identifier' => array (
					'id' => '1.1',
					'name_lt' => 'Vardas ir vieta',
					'name_en' => 'Identifier',
					'xpath' => 'lom/general/identifier',
					'limits' => '1..* (10',
					'definition_lt' => 'Bendras MO vardas ir vieta',
					'value_lt' => '',
					'value_en' => '',
					'definition2_lt' => '',
					'note_lt' => '',
					'sample_lt' => '',
					'level' => '',
					'html_type' => 'GROUP'					
					),
		'lom/general/identifier/catalog' => array (
					'id' => '1.1.1',
					'name_lt' => 'Katalogas',
					'name_en' => 'Catalog',
					'xpath' => 'lom/general/identifier/catalog',
					'limits' => '1',
					'definition_lt' => 'Įrašo atpažinimo arba katalogo sistemos vardas arba nuoroda. Vardo lauko sistema',
					'value_lt' => 'Standartas ISO/IEC 10646-1:2000',
					'value_en' => 'Repertoire of ISO/IEC 10646-1:2000',
					'definition2_lt' => '',
					'note_lt' => '',
					'sample_lt' => 'CELEBRATE, ISBN, ARIADNE, URI',
					'level' => 'Papildomas/ būtinas tik administratoriui',
					'html_type' => 'INPUT'
					),
		'lom/general/identifier/entry' => array (
					'id' => '1.1.2',
					'name_lt' => 'Vardas',
					'name_en' => 'Entry',
					'xpath' => 'lom/general/identifier/entry',
					'limits' => '1',
					'definition_lt' => 'MO vardo arba nuorodos reikšmė jo atpažinimo arba katalogų sistemoje',
					'value_lt' => 'Standartas ISO/IEC 10646-1:2000',
					'value_en' => 'Repertoire of ISO/IEC 10646-1:2000',
					'definition2_lt' => '',
					'note_lt' => '',
					'sample_lt' => 'DB123456, 2-7342-0318, LEAO875, http://foo.org/1234',
					'level' => 'Papildomas/ būtinas Tik administratoriui',
					'html_type' => 'INPUT'
					)


					);

					
	}

	function initSessionXmlInstancesFromDatabase($id, $db) {
		$rs = $db->Execute("select * from metadata where object=$id");
		$xml_db = $rs->GetArray();
		for($i=0;$i<count($xml_db); $i++) {
				$_SESSION[$xml_db[$i]['xpath'].$xml_db[$i]['item']]=array (
					'id' => $xml_db[$i]['item'],
					'key' => $xml_db[$i]['xpath'].$xml_db[$i]['xpath'],
					'value' => $xml_db[$i]['value'],
					'parent' => $xml_db[$i]['parent']					
					);									
		}		
	}

	function initXmlInstancesFromSession($xpath) {
		if(is_array($_SESSION['xml_instances'])) {
			foreach($_SESSION['xml_instances'] as $xpath1 => $value) {
					$this->xml_instances[$xpath1] = $value;
			}
		}	
	}

	function initXmlInstancesIntoSession() {
		// load from $_SESSION into $xml_instances
		$_SESSION['xml_instances'] = array (
						'lom1' => 		array ('id' => 0, 'key' => 'lom1', 'value' => '', 'parent'=>0),
						'lom/general1' =>	array ('id' => 1, 'key' => 'lom/general1', 'value' => '', 'parent'=>0),
						'lom/general/identifier1' =>	array ('id' => 2, 'key' => 'lom/general/identifier1', 'value' => '', 'parent'=>1),
						'lom/general/identifier/entry1' =>	array ('id' => 3, 'key' => 'lom/general/identifier/entry1', 'value' => 'a', 'parent'=>2),
						'lom/general2' =>	array ('id' => 4, 'key' => 'lom/general2', 'value' => '', 'parent'=>0),
						'lom/general/identifier2' =>	array ('id' => 5, 'key' => 'lom/general/identifier2', 'value' => '', 'parent'=>4),
						'lom/general/identifier/entry2' =>	array ('id' => 6, 'key' => 'lom/general/identifier/entry2', 'value' => 'b', 'parent'=>5),
						'lom/general/identifier/catalog1' =>	array ('id' => 7,  'key' => 'lom/general/identifier/catalog1', 'value' => 'c', 'parent'=>5),
						'lom/general/identifier/catalog2' =>	array ('id' => 8, 'key' => 'lom/general/identifier/catalog2', 'value' => 'c2', 'parent'=>2)
					);
	}
	
	function initXmlInstances() {
		// load from $_SESSION into $xml_instances
		$this->xml_instances = array (
						'lom1' => 		array ('id' => 0, 'key' => 'lom1', 'value' => '', 'parent'=>0),
						'lom/general1' =>	array ('id' => 1, 'key' => 'lom/general1', 'value' => '', 'parent'=>0),
						'lom/general/identifier1' =>	array ('id' => 2, 'key' => 'lom/general/identifier1', 'value' => '', 'parent'=>1),
						'lom/general/identifier/entry1' =>	array ('id' => 3, 'key' => 'lom/general/identifier/entry1', 'value' => 'a', 'parent'=>2),
						'lom/general2' =>	array ('id' => 4, 'key' => 'lom/general2', 'value' => '', 'parent'=>0),
						'lom/general/identifier2' =>	array ('id' => 5, 'key' => 'lom/general/identifier2', 'value' => '', 'parent'=>4),
						'lom/general/identifier/entry2' =>	array ('id' => 6, 'key' => 'lom/general/identifier/entry2', 'value' => 'b', 'parent'=>5),
						'lom/general/identifier/catalog1' =>	array ('id' => 7,  'key' => 'lom/general/identifier/catalog1', 'value' => 'c', 'parent'=>5),
						'lom/general/identifier/catalog2' =>	array ('id' => 8, 'key' => 'lom/general/identifier/catalog2', 'value' => 'c2', 'parent'=>2)
					);
	}
	
	function saveXmlInstancesIntoSession() {
		if(strlen($_REQUEST['xthis'])>0) {
			foreach($_SESSION['xml_instances'] as $xpath1=>$xarray1) {
				if(preg_match("!".$_REQUEST['xthis']."!",$xpath1) && is_array($_SESSION['xml_instances'][$xpath1])) {
					$_SESSION['xml_instances'][$xpath1]['value']='';
				}
			}
		}
		foreach ($_REQUEST as $item=>$value) {
			if(is_array($_SESSION['xml_instances'][$item])) {
				$_SESSION['xml_instances'][$item]['value']=$value;
			}
		}	
	}

	function saveSessionXmlInstancesToDatabase($id, $db) {
		$query = "";
		if(is_array($_SESSION['xml_instances'])) {
			foreach($_SESSION['xml_instances'] as $xpath => $value) {
					preg_match("!(\D+)(\d+)!",$xpath,$matches); // separating xpath and item id
					$query .= "INSERT INTO metadata (object, id, xpath, item, parent, value ) VALUES ($id, $value[id],'$matches[1]', $matches[2], $value[parent], '$value[value]' );\n";
			}
		}
		if($this->debug) echo $query;
	}

	/**
	* @name generateXmlForm($xpath, $parent=0)
	* calculate one level of xpath instances under the same parent and output form html
	* @param $xpath xpath which we will be counting
	* @param $parent parent id under which the xpath elements are counted
	* @return $count
	*/
	function generateXmlForm($xpath, $parent=0) {
		$i = 0; // xml instance number
		do {
			// $xml_instance_value is an Array or false
			if($this->debug) echo "<hr>generateXmlForm(xpath=$xpath, parent=$parent);\n";
			
			$i++;
			// find instance $i of $xpath xml element
			$xml_instance_value = $this->findXmlInstance($xpath, $parent, $i);
			
			// check if the item was found and if it is an instance child of the $parent
			// print_r($xml_instance_value);
			if(!is_array($xml_instance_value) && $i>1) continue;
			
			// initialize instance, if this is a first run & there is no instance in the session 
			if(!is_array($xml_instance_value) && $i==1) {
				$new_id = generateId();
				// @todo bug assigning new $xpath$i = overwrites previous value
				if($this->xml_structure[$xpath]['html_type'] !== "CHECKBOX") {
					$this->xml_instances["$xpath$new_id"] = array ('id' => $new_id, 'key' => "$xpath$new_id", 'value' => '', 'parent'=>$parent);
					$_SESSION['xml_instances']["$xpath$new_id"] = array ('id' => $new_id, 'key' => "$xpath$new_id", 'value' => '', 'parent'=>$parent);
					$xml_instance_value = array ('id' => $new_id, 'key' => "$xpath$new_id", 'value' => '', 'parent'=>$parent);
					if($this->debug) echo "<br>Adding new one: $xpath$new_id parent: $parent\n";
				} else { // CHECKBOX multiple shown items
					// $this->xml_structure[$xpath]['value_lt'] = "value1:::User friendly1,,value2:::User friendly2";
					$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
					$i=0;
					foreach($form_values as $form_value) {
						$i++;
						// Separate value from user friendly display
						if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
							$key=$matches[1];
							$display=$matches[2];
						} else { // show value id number if no value present
							$key=$i;
							$display=$form_value;
						}
					$this->xml_instances["$xpath$new_id" . "_" . $key] = array ('id' => $new_id, 'key' => "$xpath$new_id" . "_" . $key, 'value' => '', 'parent'=>$parent);
					$_SESSION['xml_instances']["$xpath$new_id" . "_" . $key] = array ('id' => $new_id, 'key' => "$xpath$new_id" . "_" . $key, 'value' => '', 'parent'=>$parent);
					$xml_instance_value = array ('id' => $new_id, 'key' => "$xpath$new_id" . "_" . $key, 'value' => '', 'parent'=>$parent);
					if($this->debug) echo "<br>Adding new one: $xpath$new_id"."_$key parent: $parent\n";
					}
				}

			} 

			if($this->mode) {
				$this->form .= $this->createXmlInstanceForm($xpath, $xml_instance_value);
			} else {
				$this->form .= $this->createXmlInstanceView($xpath, $xml_instance_value);
			}
			
			// traverse all xml_structure elements
			foreach($this->xml_structure as $xpathChild => $childValue) {
				// check if this element is a structural child
				if ($this->isXmlChild($xpath,$xpathChild)) {
						$this->generateXmlForm($xpathChild, $xml_instance_value['id']);
				}
			}
		} while ( is_array($xml_instance_value) );
	}

	/**
	* @name fileXml($xpath, $parent=0)
	* calculate one level of xpath instances under the same parent and output XML.
	* @param $xpath xpath which we will be counting
	* @param $parent parent id under which the xpath elements are counted
	* @return $count
	*/
	function fileXml($xpath, $parent=0) {
		$i = 0; // xml instance number
		do {
			// $xml_instance_value is an Array or false
			if($this->debug) echo "<hr>generateXmlForm(xpath=$xpath, parent=$parent);\n";
			
			$i++;
			// find instance $i of $xpath xml element
			$xml_instance_value = $this->findXmlInstance($xpath, $parent, $i);
			
			// check if the item was found and if it is an instance child of the $parent
			// print_r($xml_instance_value);
			if(!is_array($xml_instance_value) && $i>1) continue;
			
			// initialize instance, if this is a first run & there is no instance in the session 
			if(!is_array($xml_instance_value) && $i==1) {
				$new_id = generateId();
				// @todo bug assigning new $xpath$i = overwrites previous value
				if($this->xml_structure[$xpath]['html_type'] !== "CHECKBOX") {
					$this->xml_instances["$xpath$new_id"] = array ('id' => $new_id, 'key' => "$xpath$new_id", 'value' => '', 'parent'=>$parent);
					$_SESSION['xml_instances']["$xpath$new_id"] = array ('id' => $new_id, 'key' => "$xpath$new_id", 'value' => '', 'parent'=>$parent);
					$xml_instance_value = array ('id' => $new_id, 'key' => "$xpath$new_id", 'value' => '', 'parent'=>$parent);
					if($this->debug) echo "<br>Adding new one: $xpath$new_id parent: $parent\n";
				} else { // CHECKBOX multiple shown items
					// $this->xml_structure[$xpath]['value_lt'] = "value1:::User friendly1,,value2:::User friendly2";
					$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
					$i=0;
					foreach($form_values as $form_value) {
						$i++;
						// Separate value from user friendly display
						if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
							$key=$matches[1];
							$display=$matches[2];
						} else { // show value id number if no value present
							$key=$i;
							$display=$form_value;
						}
					$this->xml_instances["$xpath$new_id" . "_" . $key] = array ('id' => $new_id, 'key' => "$xpath$new_id" . "_" . $key, 'value' => '', 'parent'=>$parent);
					$_SESSION['xml_instances']["$xpath$new_id" . "_" . $key] = array ('id' => $new_id, 'key' => "$xpath$new_id" . "_" . $key, 'value' => '', 'parent'=>$parent);
					$xml_instance_value = array ('id' => $new_id, 'key' => "$xpath$new_id" . "_" . $key, 'value' => '', 'parent'=>$parent);
					if($this->debug) echo "<br>Adding new one: $xpath$new_id"."_$key parent: $parent\n";
					}
				}

			} 

			$this->form .= $this->fileXmlInstance($xpath, $xml_instance_value);
			
			// traverse all xml_structure elements
			foreach($this->xml_structure as $xpathChild => $childValue) {
				// check if this element is a structural child
				if ($this->isXmlChild($xpath,$xpathChild)) {
						$this->fileXml($xpathChild, $xml_instance_value['id']);
						// on return from the recursion fill in final XML values for GROUP tags
						if($childValue['html_type'] == "GROUP") {
							$this->form .= $this->fileXmlInstanceEnd($xpathChild);
						}
				}
			}
		} while ( is_array($xml_instance_value) );
	}	
	
	/**
	* @name countXmlInstances($xpath, $parent)
	* calculate one level of xpath instances under the same parent
	* @param $xpath xpath which we will be counting
	* @param $parent parent id under which the xpath elements are counted
	* @return $count
	*/
	function countXmlInstances($xpath, $parent) {
		$count=0; 
		foreach($this->xml_instances as $key=>$value) {
			if(is_array($this->xml_instances[$key]) && $this->xml_instances[$key]['parent'] == $parent && preg_match("!$xpath!", $key) && $this->xml_structure[$xpath]['html_type'] != 'CHECKBOX') {				
				$count++;
			}
		}
		return $count;
	}

	/**
	* @name xmlInstanceNumber($xpath, $parent, $xpath1)
	* count which occurence of xpath element under parent this eleement xpath1 is.
	* @param $xpath xml structure element
	* @param $parent parent id
	* @param $xpath1 child xpath+id
	* @return $count number of the xpath occurence
	*/
        function xmlInstanceNumber($xpath, $parent, $xpath1) {
                // a bug if some instance is deleted!!!

                $count=0;
                foreach($this->xml_instances as $key=>$value) {
                        if(is_array($this->xml_instances[$key]) && $this->xml_instances[$key]['parent'] == $parent && preg_match("!$xpath!", $key) && $this->xml_structure[$xpath]['html_type'] != 'CHECKBOX') {
                                $count++;
                                if($key == $xpath1) return $count;
                        }
                }
                return 0;
	}

	
	/**
	* @name findXmlInstance($xpath, $parent, $i)
	* Finds i-th occurence of xpath instance under the same parent
	* @param $xpath - path to the element
	* @param $parent - parent id
	* @param $i - occurence number of this xpath element under parent id
	* @return xml instance array if found or false otherwise.
	*/
	function findXmlInstance($xpath, $parent, $i) {
		if($this->debug) echo "\n<br>findXmlInstance(xpath=$xpath, parent=$parent, i=$i)\n";
		// traverse all xpath values till i-th value is found or end is reached
		if(!is_array($this->xml_instances)) return false;
		
		$found=0;
		$id=-1;
		// if previously deletion has been done be sure cycle will reach the last members
		// @todo the bug is: add some items, delete items in front (session size goes down) 
		// new additions will start using same ids' for the items
		foreach($this->xml_instances as $key=>$value) {
			if(is_array($this->xml_instances[$key]) && preg_match("!$xpath(\d+)!", $key, $matches)) {
				if($matches[1]<>$id) {
					$found++;
					$id=$matches[1];
					if($this->debug) echo "<br>Found found=$found requiredi=$i: xmlid=$key parent=$parent id=" . $this->xml_instances[$key]['id'] . "\n";	
					if($found==$i) return $this->xml_instances[$key];
				}
			}					
		}	
		if($this->debug) echo "\n<br>Not found\n";
		return false;
	}
	
	/**
	* @name isXmlChild($xpath1, $xpath2)
	* check if xpath2 is first level a child of xpath1
	* @param $xpath1 parent
	* @param $xpath2 child
	* @return true if child is first level child, false otherwise.
	*/
	function isXmlChild($xpath1, $xpath2) { 
		return preg_match("!^$xpath1\/\w+$!i",$xpath2);
	}
	
	/**
	* @name createXmlInstanceForm($xpath, $xml_instance_value)
	* produce a string for xpath item with parameters from xml_instance_value array
	* @param $xpath xpath in the xml structure
	* @param $xml_instance_value array containing xml element value
	* @return $form html form fragment to be included into big xml entry form
	*/
	function createXmlInstanceForm($xpath, $xml_instance_value) {
		
		if($this->debug) echo "\n<br>createXmlInstanceForm(xpath=$xpath, xml_instance_value=" . $xml_instance_value['key'];
		if(is_array($xml_instance_value)) {
			$id = $xml_instance_value['key'];
			$value = $xml_instance_value['value'];
			$parent = $xml_instance_value['parent'];
		} else {
			return "Error in createXmlInstanceForm: No XML instance for $xpath.";
		}
		$form = "\n\n<div class=row style='float:center;padding-left:" . substr_count($this->xml_structure[$xpath]['id'],".") * 100 . "px;'>";
			
		// is another instance of this lom path allowed? find max alowance, see if it is defined
		preg_match("/\d+\D+(\d+)/", $this->xml_structure[$xpath]['limits'], $matches);
		$xml_limits = $matches[1];
		$xml_items = $this->countXmlInstances($xpath, $parent);
		$xml_item_no = $this->xmlInstanceNumber($xpath, $parent, $id);
			
		if( $xml_item_no == $xml_items && strlen($xml_limits)>0 && $xml_limits>1 && $xml_items<$xml_limits && $this->xml_structure[$xpath]['html_type'] !== "CHECKBOX") { 
			$form .= " <a class=boxyellow href='object_addfield.php?xpath=$xpath&parent=$parent'>+</a> ";
		}
			
		// is deleting  this instance allowed?
		if($xml_items>1 ) { // if minimum is more than 1 - matching pattern above should be reprogrammed 
			$form .= "<a class=boxred href='object_deletefield.php?xpath=$id'>-</a> "; 
		}
		if($this->xml_structure[$xpath]['hidden']!=1){
		if(strlen($this->xml_structure[$xpath]['default_values'])<1){
		switch ($this->xml_structure[$xpath]['html_type']) {
			case "GROUP":
					$size = 3 - strlen($this->xml_structure[$xpath]['id']);
				$form .= "\n<font size=+$size><a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>:\n<font color=CCCCCC>$xpath</font></font>\n"; break;
				
			case "INPUT":
				if($xpath=='lom/relation/resource/identifier/entry'){
					// relationship search field addition
					$form .= "\n<a href=javascript:poptastic('help.php?xpath=$xpath');>".$this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>:\n<font color=CCCCCC>$xpath</font><br />\n<input type='text' name='$id' size='30' value='$value' /><A HREF='#' onclick="."open_win('./search_popup.php?field=$id')".">Surasti</A>"." ".$this->xml_structure[$xpath]['level'];
				} else	{
					$form .= "\n<a href=javascript:poptastic('help.php?xpath=$xpath');>".$this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>:\n<font color=CCCCCC>$xpath</font><br />\n<input type='text' name='$id' size='30' value='$value' /> ".$this->xml_structure[$xpath]['level'];
				}
				// remember text field id for classificator entry id input
				if($xpath=='lom/classification/taxonpath/taxon/id'){ 
					$this->classificator_entry_id=$id;
				}
				break;
			case "TEXTAREA":
				$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br />\n<textarea name='$id' cols='50' rows='10'>$value</textarea>". $this->xml_structure[$xpath]['level']; break;
			case "CONNECTED":
				if($xpath == 'lom/classification/taxonpath/taxon/entry'){
					$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br /><select name='$id' id='$id' size='10' onchange='document.forms[0].elements[\"" . $this->classificator_entry_id . "\"].value=this[this.selectedIndex].value;'>";
					$db = dbc();
					$pid = $this->classificator_name;
					$rs1 = $db->Execute("select * from classificators");
					while(!$rs1->EOF) {
						$class  = $rs1->fields['title'];
						$cid = $rs1->fields['id'];
						$classificator = new Classificator($db, $cid);
						$classificator->createForm(-1,"", "class='$class'", $value);
						$form .= $classificator->form;
						$rs1 -> MoveNext();
					}
					do {			
						$count_fields++;
						$_SESSION["count"]=$count_fields;
					} while(isset($_SESSION["script".$count_fields]));
					if(!isset($_SESSION["script".$count_fields])){
						$_SESSION["script".$count_fields] = "dynamicSelect('$pid', '$id');";
					}
				} else{
				$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br /><select name='$id' size='1'>";
				$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
				foreach($form_values as $form_value) {
					$selected = ($value==$form_value)?" selected":"";
					$form .= "<option$selected>$form_value</option>";
				}}
				$form .= "</select>". $this->xml_structure[$xpath]['level'];
				break;

			case "INTERVAL":
			case "DATE":				
			case "SELECT+A":
				$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br /><select size='1' onchange=\"document.forms[0].elements['$id'].value=this.options[this.selectedIndex].text;\">";
				$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
				foreach($form_values as $form_value) {
					$selected = ($value==$form_value)?" selected":"";
					$form .= "<option$selected>$form_value</option>";
				}
				$form .= "</select><input type='text' name='$id' size='30' value='$value' />". $this->xml_structure[$xpath]['level'];
				break;
			case "SELECT":
				if($xpath=='lom/classification/taxonpath/source'){
					$db = dbc();
					$form .=  "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br /><select name='$id' id='$id' size='1'>";
					$this->classificator_name=$id;
					$rs = $db->Execute("select * from classificators");
					 while(!$rs->EOF) {
						$selected=($value==$rs->fields['title'])?"selected":"";
						$form .= "\n<option $selected value =\"" . $rs->fields['title'] ."\">" . $rs->fields['title'] . "</option>";
						$rs->MoveNext();
					}
				} else {
					$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br /><select name='$id' size='1'>";
					$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
					foreach($form_values as $form_value) {
						$selected = ($value==$form_value)? "selected":"";
						$form .= "<option $selected>$form_value</option>";
					}
				}
				$form .= "</select>". $this->xml_structure[$xpath]['level'];
				break;
			case "CHECKBOX":
				$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br />";
				$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
				$i=0;
				$form .= "<table><tr>";
				foreach($form_values as $form_value) {
					$i++;
					if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
						$key=$matches[1];
						$display=$matches[2];
					} else {
						$key=$i;
						$display=$form_value;
					}
					$checked = ($this->xml_instances[$xpath . $xml_instance_value['id'] . "_" . $key]['value'] == 'on') ? "checked" : "";
					$form .= "<td><input type='checkbox' name='" . $xpath . $xml_instance_value['id'] . "_" . $key . "' $checked>$display</td>";
					if($i % 3 == 0) $form .= "</tr><tr>";
				}
				$form .= "</table>".$this->xml_structure[$xpath]['level'];
				break;				
			default:
				$form .= "Other: ". $this->xml_structure[$xpath]['name_lt'] . "<br />\n";
		}
		}
		else{
		//$this->xml_structure[$xpath]['html_type']="SELECT";
		$this->xml_structure[$xpath]['html_type'];
		$form .= "<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>: <font color=CCCCCC>$xpath</font><br /><select name='$id' size='1'>";
				//default values selection
				$form_values = split(",,",$this->xml_structure[$xpath]['default_values']);
				foreach($form_values as $form_value) {
					$selected = ($value==$form_value)?" selected":"";
					$form .= "<option$selected>$form_value</option>";
				}
				$form .= "</select>". $this->xml_structure[$xpath]['level'];
				//$form .= "Other: ". $this->xml_structure[$xpath]['name_lt'] . "<br />\n";		
																
		}
		$form .= $xml_instance_value['level'] . "</div><br />";
		
		
		return $form;
		}
	}

	/**
	* @name createXmlInstanceView($xpath, $xml_instance_value)
	* produce a string for xpath item with parameters from xml_instance_value array
	* @param $xpath xpath in the xml structure
	* @param $xml_instance_value array containing xml element value
	* @return $form html form fragment to be included into big xml entry form
	*/
	function createXmlInstanceView($xpath, $xml_instance_value) {
		if($this->debug) echo "\n<br>createXmlInstanceView(xpath=$xpath, xml_instance_value=" . $xml_instance_value['key'];
		if(is_array($xml_instance_value)) {
			$id = $xml_instance_value['key'];
			$value = $xml_instance_value['value'];
			$parent = $xml_instance_value['parent'];
		} else {
			return "Error in createXmlInstanceForm: No XML instance for $xpath.";
		}
		//$size = substr_count($xpath,"/") * 100;
		$tab = str_repeat("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", substr_count($xpath,"/"));
		// $form = "\n\n<div class=row style='float:center;padding-left:$size" . "px;'>";
		$form = "";
			
		// is another instance of this lom path allowed? find max alowance, see if it is defined
		preg_match("/\d+\D+(\d+)/", $this->xml_structure[$xpath]['limits'], $matches);
		$xml_limits = $matches[1];
		$xml_items = $this->countXmlInstances($xpath, $parent);
		$xml_item_no = $this->xmlInstanceNumber($xpath, $parent, $id);

		switch ($this->xml_structure[$xpath]['html_type']) {
			case "GROUP":
				$size = 3 - strlen($this->xml_structure[$xpath]['id']);
				$form .= "<tr><th colspan='2'>\n$tab<font size=+$size><a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a>"; break; // :\n<font color=CCCCCC>$xpath</font></font></th></tr>\n"; break;
				
			case "TEXTAREA":
			case "INPUT":
				if(preg_match("!^http!",$value)) {
					$value = "<a href='$value' target=_blank>" . $value . "</a>";
				}

				$form .= "\n<tr><td width=250px>$tab<a href=javascript:poptastic('help.php?xpath=$xpath');>".$this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a></td><td>$value</td></tr>\n"; break;

			case "CONNECTED":
			case "INTERVAL":
			case "DATE":				
			case "SELECT+A":
			case "SELECT":
				$form .= "<tr><td width=250px>$tab<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a></td><td>";
				$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
				foreach($form_values as $form_value) {
					if($value==$form_value) {
						$form .= $form_value . "; ";
					}
				}
				$form .= "</td></tr>";
				break;

			case "CHECKBOX":
				$form .= "<tr><td width=250px>$tab<a href=javascript:poptastic('help.php?xpath=$xpath');>" . $this->xml_structure[$xpath]['id'] . " " . $this->xml_structure[$xpath]['name_lt'] . "</a></td><td>";
				$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
				$i=0;
				foreach($form_values as $form_value) {
					$i++;
					if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
						$key=$matches[1];
						$display=$matches[2] . "; ";
					} else {
						$key=$i;
						$display=$form_value . "; ";
					}

					if($this->xml_instances[$xpath . $xml_instance_value['id'] . "_" . $key]['value'] == 'on') {
						$form .= $display;
					}
				}
				$form .= "</td></tr>";
				break;				

			default:
				// $form .= "Other: ". $this->xml_structure[$xpath]['name_lt'] . "<br />\n";
		}
		// $form .= $xml_instance_value['level'] . "</div><br />";
		return $form;
	}


	/**
	* @name saveObjectXML()
	* produce an XML string for current LOM object
	* @return $form xml fragment to be returned to client
	*/
	function saveObjectXML() {	
		$form = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
		
		$rs = $db->Execute("select * from objects left join users on (objects.author=users.id) $author order by author");
	
		while(!$rs->EOF) {
			// load form metadata
			$rs_xml = $db->Execute("select * from meta1");
			$xml_db = $rs_xml->GetArray();
			$xmlForm = new XmlInputForm($xml_db);
			$rs_xml->free();
	
			unset($_SESSION['xml_instances']);
			$object_id = $rs->fields[0];
			echo "\nExporting object $object_id";
	
			fwrite($handle,"\n<!-- Object id:" . $rs->fields[0] . " Author: " . $rs->fields['name'] . " " . $rs->fields['surname'] . " -->\n");
			$_SESSION['xml_instances'] = array (
							'lom0' => array ('id' => 0, 'key' => 'lom0', 'value' => '', 'parent'=>0)
			);
	
			$rs2 = $db->Execute("select * from metadata where object=$object_id");
			while(!$rs2->EOF) {
				$_SESSION['xml_instances'][$rs2->fields['xpath']] = array (
							'id' => $rs2->fields['item'],
							'key' => $rs2->fields['xpath'],
							'value' => $rs2->fields['value'],
							'parent' => $rs2->fields['parent']					
							);				
				$rs2 -> MoveNext();
			}
	
			if($rs->fields['file_size']>0) {
				$_SESSION['xml_instances']['uploadedfile']['name'] = $rs->fields['file_name'];
				$_SESSION['xml_instances']['uploadedfile']['type'] = $rs->fields['file_type'];
				$_SESSION['xml_instances']['uploadedfile']['size'] = $rs->fields['file_size'];
				$_SESSION['xml_instances']['uploadedfile']['path'] = $rs->fields['file_path'];
			}
			$_SESSION['xml_instances']['author']=$rs->fields['author'];
		
			$xmlForm->initXmlInstancesFromSession("lom"); // initialize object from session
			$xmlForm->fileXml("lom"); // generate form
			// echo $xmlForm->form;
			fwrite($handle,"\n<lom xmlns=\"http://ltsc.ieee.org/xsd/LOM\">" . $xmlForm->form . "\n</lom>\n\n");
			$rs -> MoveNext();
		}
	}


	/**
	* @name fileXmlInstance($xpath, $xml_instance_value)
	* produce an XML string for xpath item with parameters from xml_instance_value array
	* @param $xpath xpath in the xml structure
	* @param $xml_instance_value array containing xml element value
	* @return $form xml fragment to be included into big xml record
	*/
	function fileXmlInstance($xpath, $xml_instance_value) {
		if($this->debug) echo "\n<br>createXmlInstanceForm(xpath=$xpath, xml_instance_value=" . $xml_instance_value['key'];
		if(is_array($xml_instance_value)) {
			$id = $xml_instance_value['key'];
			$value = $xml_instance_value['value'];
			$parent = $xml_instance_value['parent'];
		} else {
			return "Error in createXmlInstanceForm: No XML instance for $xpath.";
		}
		$pad = str_pad('', substr_count($this->xml_structure[$xpath]['id'],".") + 1, "\t");
		$form = "\n$pad"; // pad tabs from the left - making nice xml
		
		// calculate last xml item
		preg_match("!.*/(\D+)!", $xpath, $matches);
		$xml_tag=$matches[1];

		// case sensitive LOM standard form
		preg_match("!.*/(\D+)!", $this->xml_structure[$xpath]['xpath2'], $matches);
		$xml_tag2=$matches[1];
		if(strlen($xml_tag)==0) return $form;

		// set pre and post tags for current xpath, swallows subtags
/*		if(strpos($xpath,"general/identifier")!==false || strpos($xpath,"general/language")!==false ||
		strpos($xpath,"lifecycle/status")!==false || strpos($xpath,"lifecycle/contribute")!==false || 
		strpos($xpath,"technical/format")!==false || strpos($xpath,"technical/size")!==false || 
		strpos($xpath,"technical/location")!==false || strpos($xpath,"technical/requirement")!==false || 
		strpos($xpath,"educational/language")!==false ||
		strpos($xpath,"relation/resource/identifier")!==false ||
		strpos($xpath,"annotation/entity")!==false ||
		strpos($xpath,"classification/taxonpath/taxon/id")!==false) {
			$pre_tag = "";
			$post_tag = "";
		} else {
			$pre_tag = "<language>lt</language>\n\t$pad<value>";
			$post_tag = "</value>";
		} 
*/
		// keyword extension
/*		if(strpos($xpath,"general/keyword")!==false) {
			$xml_instance_value2 = $this->findXmlInstance("lom/classification/taxonpath/taxon/entry", 0, 1);
			$pre_tag .= " " . $xml_instance_value2['value'] . " aaaaaaaaaa ";
		}
*/

		// taxonomy tagging
		if(strpos($xpath,"lom/classification/taxonpath/taxon/entry")!==false || strpos($xpath,"lom/classification/taxonpath/source")!==false) {
			$pre_tag = "<string language=\"lt\">";
			$post_tag = "</string>";
		}
		// "<source></source><value>...</value>" tagging
		else if(strpos($xpath,"general/structure")!==false || strpos($xpath,"general/aggregationlevel")!==false ||
		strpos($xpath,"lifecycle/status")!==false || strpos($xpath,"lifecycle/contribute/role")!==false ||
		strpos($xpath,"technical/requirement/orcomposite/type")!==false ||
		strpos($xpath,"technical/requirement/orcomposite/name")!==false ||
		strpos($xpath,"educational/interactivitytype")!==false || strpos($xpath,"educational/learningresourcetype")!==false ||
		strpos($xpath,"educational/interactivitylevel")!==false || strpos($xpath,"educational/semanticdensity")!==false ||
		strpos($xpath,"educational/intendedenduserrole")!==false || strpos($xpath,"educational/context")!==false ||
		strpos($xpath,"educational/difficulty")!==false ||
		strpos($xpath,"rights/cost")!==false || strpos($xpath,"rights/copyrightandotherrestrictions")!==false ||
		strpos($xpath,"relation/kind")!==false || 
		strpos($xpath,"classification/purpose")!==false
		) {
			// $pre_tag = "<source>http://lom.emokykla.lt/vocabs</source>\n\t$pad<value>";
			$pre_tag = "<source>LOMv1.0</source>\n\t$pad<value>";
			$post_tag = "</value>";
		}

		// "<datetime>...</datetime> value tagging
		else if(strpos($xpath,"lifecycle/contribute/date")!==false || strpos($xpath,"annotation/date")!==false) {
			$pre_tag = "<dateTime>";
			$post_tag = "</dateTime>";
		}

		// "<duration>...</duration>" value tagging
		else if(strpos($xpath,"educational/typicallearningtime")!==false || 
		strpos($xpath,"technical/duration")!==false ) {
			$pre_tag = "<duration>";
			$post_tag = "</duration>";
		}

		switch ($this->xml_structure[$xpath]['html_type']) {
			case "GROUP":
				$form .= "<$xml_tag2>"; break;
				
			case "TEXTAREA":
			case "INPUT":
			case "CONNECTED":
			case "INTERVAL":
			case "DATE":				
			case "SELECT+A":
			case "SELECT":
				$form .= "<$xml_tag2$in_tag>\n$pad\t$pre_tag$value$post_tag\n$pad</$xml_tag2>"; break;
			case "CHECKBOX":
				$form_values = split(",,",$this->xml_structure[$xpath]['value_lt']);
				$i=0;
				foreach($form_values as $form_value) {
					$i++;
					if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
						$key=$matches[1];
						$display=$matches[2];
					} else {
						$key=$i;
						$display=$form_value;
					}
					if($this->xml_instances[$xpath . $xml_instance_value['id'] . "_" . $key]['value'] == 'on') {
						$form .= "<$xml_tag2$in_tag>\n$pad\t$pre_tag$key$post_tag\n$pad</$xml_tag2>";
					}
				}
				break;				
			default:
		}
		return $form;
	}

	/**
	* @name fileXmlInstanceEnd($xpath, $xml_instance_value)
	* produce an XML string for xpath item with parameters from xml_instance_value array
	* @param $xpath xpath in the xml structure
	* @param $xml_instance_value array containing xml element value
	* @return $form xml fragment to be included into big xml record
	*/
	function fileXmlInstanceEnd($xpath) {
		$form .= "\n" . str_pad('', substr_count($this->xml_structure[$xpath]['id'],".") + 1, "\t"); // pad tabs from the left - making nice xml
		preg_match("!.*/(\D+)!", $this->xml_structure[$xpath]['xpath2'], $matches); // finding the last tag in the xpath
		$form .= "</" . $matches[1] . ">";
		return $form;;
	}
}

/*
	global $CFG;
	include("../include/config.php");		

	$xmlForm = new XmlInputForm(false);
	$xmlForm->test();
*/

?>
