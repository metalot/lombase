<?php
	include("../include/config_admin.php");
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		main => "edit_wizard.html",
		footer => "footer.html"
	));	
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	$rs = $db->Execute("select id,name_lt,default_values,hidden from meta1");
	$n=1;
	while(!$rs->EOF) {
	$checked=" ";
	if($rs->fields['id']!='Nr.'){
	$tpl -> assign(ID, $n);
	$tpl -> assign(ID1, $rs->fields['id']);
	$tpl -> assign(NAME, $rs->fields['name_lt']);
	if($rs->fields['hidden']==1){$checked="checked='checked'";}
	else{$checked=" ";}
	if(strlen($rs->fields['default_values'])>1){
	$values = $rs->fields['default_values']; }
	else{$values = " ";}
	$tpl -> assign(VALUES, $values);
	$tpl -> assign(CHECKED, $checked);
	$tpl -> parse(viewlist, ".view");
	$n++;
	}
	
	$rs -> MoveNext();}	
	
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>