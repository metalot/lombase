<?php
	include("../include/config_user.php");

	if (!isset($_REQUEST['search'])) {
		header("Location: index.php");
		exit;
	}

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header.html",
		main => "search_result_for_admin.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"class=\"active\"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> define_dynamic("view", "main");
	
	$db = dbc();
	
	$optimalsite_administrators = "optimalsite_administrators";
	$lombase_objects = "objects";
		
	$rs = $db->Execute("select * from metadata where value LIKE '%" . $_REQUEST['search'] ."%' GROUP BY object");
	while(!$rs->EOF) {
		$object_id = $rs->fields['object']; 
		$tpl -> assign(ID, $object_id);
		$rs2 = $db->Execute("select * from metadata where object=$object_id and xpath LIKE 'lom/general/title%'");
		$tpl -> assign(TITLE_SHORT, $rs2->fields['value']);
		$rs2 -> close();
		
		$query = "SELECT * from metadata MT, {$optimalsite_administrators} AD WHERE object=$object_id and MT.author=AD.id and xpath LIKE 'lom/general/description%'";
		$rs3 = $db->Execute($query);
		$tpl -> assign(TITLE_LONG, $rs3->fields['value']);
		$tpl -> assign(AUTHOR, $rs3->fields['firstname'] . " " . $rs3->fields['lastname'] . " (" . $rs3->fields['username'] . ")");
		$rs3 -> close();
				
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
