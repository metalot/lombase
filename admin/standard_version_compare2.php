<?php
	include("../include/config_admin.php");
	include("../include/class.Classificator.php");
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_classificator.html",
		main => "standard_version_compare2.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	$db = dbc();
	
	$v1 = $db->GetArray("SELECT xpath, id, name_lt FROM meta1 WHERE version=" . $_REQUEST['v1']);
	$v2 = $db->GetAssoc("SELECT xpath, id, name_lt FROM meta1 WHERE version=" . $_REQUEST['v2']);

	$j=0;
	$comparison_table = "<table><tr><th colspan=2>LOM v.".$_REQUEST['v1']."</th><th colspan=2>LOM v.".$_REQUEST['v2']."</th></tr> <tr><td>id</td><td>xpath</td><td>id</td><td>xpath</td></tr>";
	for($i=0;$i<count($v1); $i++) {
		$bgcolor="white";
		if(strlen($v1[$i]['id'])==1) $bgcolor="yellow";
		$comparison_table .= "<tr bgcolor=$bgcolor><td>" . $v1[$i]['id'] . "</td><td>" . $v1[$i]['name_lt'] . "</td><td>";
		if(strlen($v2[$v1[$i]['xpath']])>0) {
			$comparison_table .= $v2[$v1[$i]['xpath']]['id'] . "</td><td>" . $v1[$i]['name_lt'] . "</td></tr>";
			$v2[$v1[$i]['xpath']]['checked']=1;
		} else {
			$comparison_table .= "None</td><td bgcolor=red>None</td></tr>";
		}
	}
	$comparison_table .= "</table>";
	
	$tpl->assign(COMPARISON,$comparison_table);
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
