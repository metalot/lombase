<?php
	include("../include/config_admin.php");
	include("../include/class.Classificator.php");
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_classificator.html",
		main => "classificator_mapping2.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	$db = dbc();
	
	$c2 = $db->GetArray("SELECT * FROM classificator_items WHERE cid=" . $_REQUEST['c2']);
	$mappings = $db->GetArray("SELECT id1, id2 FROM classificator_mapping WHERE cid1=" . $_REQUEST['c1'] . " AND cid2=" . $_REQUEST['c2']);
	$c1 = new Classificator($db, $_REQUEST['c1']);
	$c1->createMapping(-1, "", $mappings, $c2);

	$tpl->assign(CLASSIFICATOR_MAPPING, $c1->form);
	$tpl->assign(CLASSIFICATOR1, $_REQUEST['c1']);
	$tpl->assign(CLASSIFICATOR2, $_REQUEST['c2']);
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
