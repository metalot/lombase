<?php
	include("../include/config_admin.php");

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_classificator.html",
		main => "classificator_list.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	
	$optimalsite_administrators = "users";
	$lombase_classificators = "classificators";
	
	$query = "SELECT CL.*, AD.name, AD.surname FROM {$lombase_classificators} CL, {$optimalsite_administrators} AD WHERE CL.author = AD.id";
	$rs = $db->Execute($query);

	while(!$rs->EOF) {
		$tpl -> assign(CLASSIFICATOR_ID, 	$rs->fields['id']);
		$tpl -> assign(CLASSIFICATOR_TITLE, $rs->fields['title']);
		$tpl -> assign(CLASSIFICATOR_VERSION, $rs->fields['version']);
		$tpl -> assign(CLASSIFICATOR_AUTHOR, $rs->fields['firstname'] . " " . $rs->fields['lastname'] . " (" . $rs->fields['author'] . ")");
		$tpl -> assign(CLASSIFICATOR_SEARCH_ENABLED, ($rs->fields['searchable']==1) ? "Taip" : "Ne");
		$tpl -> assign(CLASSIFICATOR_MAPPING, ($rs->fields['mapping']==1) ? "Taip" : "Ne");
		$tpl -> assign(CLASSIFICATOR_CREATED, $rs->fields['created']);
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
