<?php
	include("../include/config_admin.php");

	
	if(strlen($_GET['author'])>0) {
		$author = " where OB.author=" . $_GET['author'];
	} else {
		$author = "";
	}

	// clean data from previous object view
	unset($_SESSION['xml_instances']);
	unset($_SESSION['object_id']);
	unset($_SESSION['language']);

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		main => "object_list.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"class=\"active\"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	
	$optimalsite_administrators = "users";
	$lombase_objects = "objects";
	$lombase_classificators = "classificators";
	
	$query = "SELECT OB.id as id, AD.login as username FROM {$lombase_objects} OB LEFT JOIN {$optimalsite_administrators} AD on (OB.author = AD.id) $author ORDER by OB.author";
	$rs = $db->Execute($query);

	while(!$rs->EOF) {
		$rs2 = $db->Execute("select * from metadata where object=".$rs->fields['id']." and language='" .$CFG->default_metadata_language. "' and xpath LIKE 'lom/general/title%'");
		$tpl -> assign(ID, 	$rs->fields['id']);
		$tpl -> assign(TITLE_SHORT, $rs2->fields['value']);
		$tpl -> assign(AUTHOR, $rs->fields['username']);
		$rs1 = $db->Execute("select av(rating) from objects_rating where $object_id=object_id");
		$rating = $rs1->fields["avg(rating)"];
		$rating_rounded = round($rating, 2);
		$tpl -> assign(AVG, $rating_rounded);
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
		
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
