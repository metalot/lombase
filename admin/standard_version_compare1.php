<?php
	include("../include/config_admin.php");
	include("../include/class.Classificator.php");
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_standard.html",
		main => "standard_version_compare1.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	$db = dbc();
	
	$versions = "";
	$classificator_versions = $db->GetArray("SELECT version FROM meta1 group by version");
	for($i=0;$i<count($classificator_versions); $i++) {
		$versions .= "<option value=" . $classificator_versions[$i]['version'] . ">LOM v." . $classificator_versions[$i]['version'] . "</option>";
	}
	
	$tpl->assign(VERSION1, $versions);
	$tpl->assign(VERSION2, $versions);
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
