<?php
	include("../include/config_admin.php");
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		main => "edit_version.html",
		footer => "footer.html"
	));	
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	$n=1;
	if(isset($_GET["version"])){
	$version = $_GET["version"];
	}
	else{
	$version = 1;
	}
	if(isset($_GET["version_new"])){
	$tpl -> assign(NEW_VERSION, '&version_new='.$_GET["version_new"]);
	}
	else{
	$tpl -> assign(NEW_VERSION,'');
	}
	
	$rs = $db->Execute("select * from meta1 where version='$version' order by id asc");
	while(!$rs->EOF) {
	if($rs->fields['id']!='Nr.'){
	$n++;
	$tpl -> assign(N, $n);
	$tpl -> assign(ID, $rs->fields['id']);
	$tpl -> assign(NAME_LT, $rs->fields['name_lt']);
	$tpl -> assign(NAME_EN, $rs->fields['name_en']);
	$tpl -> assign(XPATH, $rs->fields['xpath']);
	$tpl -> assign(XPATH2, $rs->fields['xpath2']);
	$tpl -> assign(LIMITS, $rs->fields['limitis']);
	$tpl -> assign(DEFINITION_LT, $rs->fields['definition_lt']);
	$tpl -> assign(DEFINITION_EN, $rs->fields['definition_en']);
	$tpl -> assign(VALUE_LT, $rs->fields['value_lt']);
	$tpl -> assign(VALUE_EN, $rs->fields['value_en']);
	$tpl -> assign(DEFINITION2_LT, $rs->fields['definition2_lt']);
	$tpl -> assign(DEFINITION2_EN, $rs->fields['definition2_en']);
	$tpl -> assign(NOTE_LT, $rs->fields['note_lt']);
	$tpl -> assign(SAMPLE_LT, $rs->fields['sample_lt']);
	$tpl -> assign(LEVEL, $rs->fields['level']);
	$tpl -> assign(HTML_TYPE, $rs->fields['html_type']);
	$tpl -> assign(SOURCE, $rs->fields['source']);
	$tpl -> parse(viewlist, ".view");
	}
	
	$rs -> MoveNext();
	}	
	
	$tpl -> define_dynamic("view1", "main");
	$rs = $db->Execute("select * from meta1 order by version asc");
	while(!$rs->EOF) {
	if(($rs->fields['id']!='Nr.')&&($version_html != $rs->fields['version'])){
	$version_html = $rs->fields['version'];
	$tpl -> assign(VERSION, $version_html);
	
		$tpl -> parse(viewlist1, ".view1");
	}
	
	$rs -> MoveNext();
	}	
	$version_html++;
	$tpl -> assign(VERSION_NEW, $version_html);
	if(isset($_GET["version"])){
	$selected = $_GET["version"];
	$tpl->assign(SELECTED.$_GET["version"], 'selected="selected"');
	$tpl->assign(CURRENT_VERSION, $_GET["version"]);
	}
	else{
	$selected = 1;
	$tpl->assign(SELECTED1, 'selected="selected"');
	$tpl->assign(CURRENT_VERSION, 1);
	
	}
	
	
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>