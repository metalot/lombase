<?php
	include("../include/config.php");
	
	$grp = $_SESSION['grp'];
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
		$tpl -> define( array(
		head => "header_admin.html",
		main => "comment_edit_a.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"class=\"active\"");
	$tpl->assign(ACTIVE_7,"");
	$tpl -> define_dynamic("view", "main");
	$com_id=$_GET["com_id"];
	$db = dbc();
	$rs = $db->Execute("SELECT * FROM commentary where com_id=$com_id");
	$tpl -> assign(COM_ID, $rs->fields['com_id']);
	$tpl -> assign(USER, $rs->fields['user_name']);
	$tpl -> assign(EMAIL, $rs->fields['user_email']);
	$tpl -> assign(DATE, $rs->fields['date']);
	$tpl -> assign(COM_TEXT, $rs->fields['com_text']);
	if(isset($_GET["edited"])){
	$com_text = $_POST["textfield"];
	$com_id = $_GET["com_id"];
	$db = dbc();
	$rs1 = $db->Execute("update commentary set com_text='$com_text' where com_id=$com_id");
	header("location: comment_edit.php?com_id=$com_id");
	}
	
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>	