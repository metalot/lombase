<?

include("../include/config.php");
include("../include/xml_struct.php");
// Export one big xml file full of descriptors for all 
// 1. Select all objects
// 2. Move over each object and
// 2.1 select all metameta information
// 2.2 select value for metadata item
// 2.3 print xml item

        if(strlen($_GET['limit'])>0) { $limit=$_GET['limit'] . ",100"; } else { $limit="100"; }
	$go = false;
	
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Content-type: text/plain;\n\n");
	$db = dbc();
	
	$rs = $db->Execute("select * from objects left join users on (objects.author=users.id) $author order by author limit $limit");
	
	while(!$rs->EOF) {
		$object_id = $rs->fields[0];
		$rights = $db->GetOne("select value from metadata where object=$object_id and xpath like 'lom/rights/description%'");
		if($rights == "Be eksporto teisės") {
			echo "\nObject id: $object_id Author: " . $rs->fields['name'] . " " . $rs->fields['surname'] . " no export rights";
			$rs -> MoveNext();
			continue;
		}
		
		$handle = fopen("/tmp/object_id_$object_id.xml", "w+");
		fwrite($handle,"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");

		$go = true;
		
		unset($_SESSION['xml_instances']);
		echo "\nExporting object $object_id";
		fwrite($handle,"\n<!-- Object id:" . $rs->fields[0] . " Author: " . $rs->fields['name'] . " " . $rs->fields['surname'] . " -->\n");		
		$_SESSION['xml_instances'] = array (
						'lom0' => array ('id' => 0, 'key' => 'lom0', 'value' => '', 'parent'=>0)
		);
		$rs2 = $db->Execute("select * from metadata where object=$object_id");
		while(!$rs2->EOF) {
			$_SESSION['xml_instances'][$rs2->fields['xpath']] = array (
						'id' => $rs2->fields['item'],
						'key' => $rs2->fields['xpath'],
						'value' => $rs2->fields['value'],
						'parent' => $rs2->fields['parent']					
						);				
			$rs2 -> MoveNext();
		}

		if($rs->fields['file_size']>0) {
			$_SESSION['xml_instances']['uploadedfile']['name'] = $rs->fields['file_name'];
			$_SESSION['xml_instances']['uploadedfile']['type'] = $rs->fields['file_type'];
			$_SESSION['xml_instances']['uploadedfile']['size'] = $rs->fields['file_size'];
			$_SESSION['xml_instances']['uploadedfile']['path'] = $rs->fields['file_path'];
		}
		$_SESSION['xml_instances']['author']=$rs->fields['author'];

		// load form metadata
		$xml_db = $db->GetArray("select * from meta1");
		$xmlForm = new XmlInputForm($xml_db);
		$xmlForm->initXmlInstancesFromSession("lom"); // initialize object from session
		$xmlForm->fileXml("lom"); // generate form
		// echo $xmlForm->form;
		fwrite($handle,"\n<lom xmlns=\"http://www.imsglobal.org/xsd/imsmd_rootv1p2p1\">\n" . 
		   "\t<metaMetadata>\n\t<contribute>\n\t\t<role>\n\t\t\t<source>LREv3.0</source>\n\t\t\t<value>provider</value>\n\t\t</role>\n\t\t<entity>begin:vcard\n\t\torg:Lithuania ministry of education;Information technology center\n\t\tversion:2.1\n\t\tend:vcard\n\t\t</entity>\n\t</metaMetadata>" .
		   $xmlForm->form . "\n</lom>\n\n");		
		$rs -> MoveNext();
		fclose($handle);
	}
	
	echo "\ndone.";
	if($go) {
		$start = $_GET['limit'] + 100;
		// header("Location: http://lom.emokykla.lt/admin/xml_export.php?limit=$start");
	}
	exit;

?>
