<?php
	include("../include/config_admin.php");

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_user.html",
		main => "user_list.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"class=\"active\"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	
	$optimalsite_administrators = "users";
	$lombase_objects = "objects";
	
	$query = "SELECT AD.id, AD.login, AD.name, AD.surname, count(OB.author) from {$optimalsite_administrators} AD LEFT JOIN {$lombase_objects} OB on (AD.id = OB.author) group by AD.login order by AD.login";
	
	$rs = $db->Execute($query);
	while(!$rs->EOF) {
		// Valid login
		$tpl -> assign(ID, 	$rs->fields['id']);
		$tpl -> assign(LOGIN, 	$rs->fields['login']);
		$tpl -> assign(NAME, $rs->fields['name']);
		$tpl -> assign(SURNAME, $rs->fields['surname']);
		$tpl -> assign(OBJNUMBER, "<a href=object_list.php?author=" . $rs->fields['id'] . ">".$rs->fields[4] . "</a>");
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
	//count quantity of users
	$counter=0;
	$counter=$db->GetOne("select COUNT(id) from users");
	
	$tpl -> assign(COUNTER, 	$counter);
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
