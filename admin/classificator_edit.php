<?php
	include("../include/config_admin.php");
	include("../include/class.Classificator.php");
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_classificator.html",
		main => "classificator_edit.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	// template variable initialization
	$tpl -> assign(CLASSIFICATOR_SEARCH_ENABLED, "");
	
	if (isset($_REQUEST['cid'])) {
		$db = dbc();
		$classificator = new Classificator($db, $_REQUEST['cid']);
		$classificator->createForm();
		$tpl -> assign(CLASSIFICATOR_TITLE, $classificator->title);
		if($classificator->searchable>0) $tpl -> assign(CLASSIFICATOR_SEARCH_ENABLED, "checked");
		if($classificator->mapping>0) $tpl -> assign(CLASSIFICATOR_MAPPING, "checked");
		$tpl -> assign(CLASSIFICATOR_OUTPUT, $classificator->form);
		$tpl -> assign(CLASSIFICATOR_ID, $_REQUEST['cid']);
	} else {
		$tpl -> assign(CLASSIFICATOR_OUTPUT,"");
	}
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
