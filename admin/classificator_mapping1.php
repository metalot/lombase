<?php
	include("../include/config_admin.php");
	include("../include/class.Classificator.php");
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_classificator.html",
		main => "classificator_mapping1.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	$db = dbc();
	
	$query = "SELECT * FROM classificators";
	$rs = $db->Execute($query);
	$classificators1 = "";
	$classificators2 = "";

	while(!$rs->EOF) {
		if($rs->fields['mapping']==0) {
			$classificators1 .= "<option value=" . $rs->fields['id'] . ">" . $rs->fields['title'] . "</option>";
		} else {
			$classificators2 .= "<option value=" . $rs->fields['id'] . ">" . $rs->fields['title'] . "</option>";
		}
		$rs -> MoveNext();
	}
	$tpl->assign(CLASSIFICATORS1,$classificators1);
	$tpl->assign(CLASSIFICATORS2,$classificators2);
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
