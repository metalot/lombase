<?php
	include("../include/config_admin.php");
	if(!isset($_GET['userId'])) {
		header("Location: index.php");
	} else {
		$userId = $_GET['userId'];
	}

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_user.html",
		main => "user_view.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"class=\"active\"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$db = dbc();
	$rs = $db->Execute("select * from users where id=" . $userId);
	$tpl -> assign(ID, 	$rs->fields['id']);
	$tpl -> assign(LOGIN, 	$rs->fields['login']);
	$tpl -> assign(NAME, $rs->fields['name']);
	$tpl -> assign(SURNAME, $rs->fields['surname']);
	$tpl -> assign(EMAIL, $rs->fields['email']);
	$tpl -> assign(GRP, $rs->fields['grp']);	

	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
