<?php
	include("../include/config_admin.php");
	$userId = $_REQUEST['userId'];
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header_admin.html",
		submenu => "submenu_admin_user.html",
		main => "user_edit.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"class=\"active\"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	if (isset($_REQUEST['userId'])) {
		$db = dbc();
		$rs = $db->Execute("select * from users where id=" . $userId);
		$tpl -> assign(ID, 	$rs->fields['id']);
		$tpl -> assign(LOGIN, 	$rs->fields['login']);
		$tpl -> assign(PSW, 	$rs->fields['password']);
		$tpl -> assign(NAME, $rs->fields['name']);
		$tpl -> assign(SURNAME, $rs->fields['surname']);
		$tpl -> assign(EMAIL, $rs->fields['email']);
		if($rs->fields['grp'] == "a") {
			$tpl -> assign(GRP_U, "");
			$tpl -> assign(GRP_A, "selected");
		} else {
			$tpl -> assign(GRP_U, "selected");
			$tpl -> assign(GRP_A, "");
		}
	} else {
		$tpl -> assign(ID, "");
		$tpl -> assign(LOGIN, "");
		$tpl -> assign(PSW, "");
		$tpl -> assign(NAME, "");
		$tpl -> assign(SURNAME, "");
		$tpl -> assign(EMAIL, "");
		$tpl -> assign(GRP, "");	
	}

	$tpl -> parse(HEAD, "head");
	$tpl -> parse(SUBMENU, "submenu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
