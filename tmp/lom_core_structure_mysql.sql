-- phpMyAdmin SQL Dump
-- version 2.9.1.1
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jul 24, 2007 at 10:28 PM
-- Server version: 4.1.22
-- PHP Version: 4.4.7-pl0-gentoo
-- 
-- Database: `lom`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `log`
-- 

CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `user_id` int(10) unsigned NOT NULL default '0',
  `object_id` int(10) unsigned NOT NULL default '0',
  `update` mediumblob NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `meta1`
-- 

CREATE TABLE `meta1` (
  `id` varchar(32) character set utf8 collate utf8_bin NOT NULL default '',
  `name_lt` varchar(254) NOT NULL default '',
  `name_en` varchar(254) character set utf8 collate utf8_bin NOT NULL default '',
  `xpath` varchar(254) character set utf8 collate utf8_bin NOT NULL default '',
  `xpath2` varchar(254) character set utf8 collate utf8_bin NOT NULL default '',
  `limits` varchar(16) character set utf8 collate utf8_bin NOT NULL default '',
  `definition_lt` varchar(254) NOT NULL default '',
  `value_lt` text NOT NULL,
  `value_en` text NOT NULL,
  `definition2_lt` varchar(254) NOT NULL default '',
  `note_lt` varchar(254) NOT NULL default '',
  `sample_lt` varchar(254) NOT NULL default '',
  `level` varchar(64) character set utf8 collate utf8_bin NOT NULL default '',
  `html_type` varchar(16) character set utf8 collate utf8_bin NOT NULL default '',
  PRIMARY KEY  (`xpath`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table `metadata`
-- 

CREATE TABLE `metadata` (
  `parent` int(10) unsigned NOT NULL default '0' COMMENT 'parent object id',
  `object` smallint(6) unsigned NOT NULL default '0' COMMENT 'object id to which this metadata belongs',
  `xpath` varchar(254) collate utf8_bin NOT NULL default '' COMMENT 'xpath to this metadata item',
  `item` int(10) unsigned default '0' COMMENT 'metadata item number, where multiple occurence allowed',
  `value` text character set utf8 collate utf8_unicode_ci NOT NULL COMMENT 'metadata item value',
  `author` smallint(6) unsigned NOT NULL default '0' COMMENT 'author user id',
  `language` varchar(2) collate utf8_bin NOT NULL default 'lt' COMMENT 'metadata language',
  PRIMARY KEY  (`object`,`xpath`,`language`),
  KEY `xpath` (`xpath`(64)),
  KEY `object` (`object`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

-- 
-- Table structure for table `objects`
-- 

CREATE TABLE `objects` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `author` int(11) default NULL,
  `modified` timestamp NULL default NULL on update CURRENT_TIMESTAMP,
  `file_name` varchar(255) character set latin1 collate latin1_bin NOT NULL default '',
  `file_type` varchar(255) character set latin1 collate latin1_bin NOT NULL default '',
  `file_size` int(10) unsigned NOT NULL default '0',
  `file_data` mediumblob NOT NULL,
  `file_path` varchar(255) character set ascii collate ascii_bin default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1429 ;

-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

CREATE TABLE `users` (
  `id` int(16) unsigned NOT NULL auto_increment,
  `login` varchar(16) NOT NULL default '',
  `password` varchar(32) NOT NULL default '',
  `name` varchar(32) character set utf8 collate utf8_bin default NULL,
  `surname` varchar(32) character set utf8 collate utf8_bin default NULL,
  `email` varchar(64) default NULL,
  `address1` varchar(128) default NULL,
  `address2` varchar(128) default NULL,
  `country` varchar(32) default NULL,
  `grp` char(1) default 'u',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;
