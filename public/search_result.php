﻿<?php
	include("../include/config.php");
	$page = $_GET["page"];
	$limit_max = $page*10;
	$limit_min = $limit_max - 10;
	$current_limit  = " LIMIT $limit_min".','."$limit_max ";
	if (isset($_REQUEST['search'])) {
		$_SESSION["search"] = $_REQUEST['search'];
	} else{
		$_REQUEST['search'] = $_SESSION["search"];
	}
	if (isset($_REQUEST['from_year'])) {
		$_SESSION["from_year"] = $_REQUEST['from_year'];
	} else{
		$_REQUEST['from_year'] = $_SESSION["from_year"];
	}
	if (isset($_REQUEST['from_month'])) {
		$_SESSION["from_month"] = $_REQUEST['from_month'];
	} else{
		$_REQUEST['from_month'] = $_SESSION["from_month"];
	}
		if (isset($_REQUEST['from_day'])) {
		$_SESSION["from_day"] = $_REQUEST['from_day'];
	} else{
		$_REQUEST['from_day'] = $_SESSION["from_day"];
	}
	if (isset($_REQUEST['to_year'])) {
		$_SESSION["to_year"] = $_REQUEST['to_year'];
	} else{
		$_REQUEST['to_year'] = $_SESSION["to_year"];
	}
	if (isset($_REQUEST['to_month'])) {
		$_SESSION["to_month"] = $_REQUEST['to_month'];
	} else{
		$_REQUEST['to_month'] = $_SESSION["to_month"];
	}
	if (isset($_REQUEST['to_day'])) {
		$_SESSION["to_day"] = $_REQUEST['to_day'];
	} else{
		$_REQUEST['to_day'] = $_SESSION["to_day"];
	}
	if (isset($_REQUEST['lom/general/language'])) {
		$_SESSION['lom/general/language'] = $_REQUEST['lom/general/language'];
	} else{
		$_REQUEST['lom/general/language'] = $_SESSION['lom/general/language'];
	}
	if (isset($_REQUEST['lom/educational/context'])) {
		$_SESSION['lom/educational/context'] = $_REQUEST['lom/educational/context'];
	} else{
		$_REQUEST['lom/educational/context'] = $_SESSION['lom/educational/context'];
	}
	if (isset($_REQUEST['agefrom'])) {
		$_SESSION['agefrom'] = $_REQUEST['agefrom'];
	} else{
		$_REQUEST['agefrom'] = $_SESSION['agefrom'];
	}
	if (isset($_REQUEST['lom/technical/format'])) {
		$_SESSION['lom/technical/format'] = $_REQUEST['lom/technical/format'];
	} else{
		$_REQUEST['lom/technical/format'] = $_SESSION['lom/technical/format'];
	}
		if (isset($_REQUEST['lom/rights/cost'])) {
		$_SESSION['lom/rights/cost'] = $_REQUEST['lom/rights/cost'];
	} else{
		$_REQUEST['lom/rights/cost'] = $_SESSION['lom/rights/cost'];
	}
		if (isset($_REQUEST['lom/lifecycle/contribute/entity'])) {
		$_SESSION['lom/lifecycle/contribute/entity'] = $_REQUEST['lom/lifecycle/contribute/entity'];
	} else{
		$_REQUEST['lom/lifecycle/contribute/entity'] = $_SESSION['lom/lifecycle/contribute/entity'];
	}
		if (isset($_REQUEST['ageto'])) {
		$_SESSION['ageto'] = $_REQUEST['ageto'];
	} else{
		$_REQUEST['ageto'] = $_SESSION['ageto'];
	}

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "public_header.html",
		main => "search_rezult_public.html",
		menu=> "menu.html",
		footer => "public_footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"class=\"active\"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> define_dynamic("view", "main");
	
	$db = dbc();
	$object_list = "-1"; // restart object list minimization

	// TAXONOMY search
	foreach($_REQUEST as $name=>$parameter) { 
		// refining the object list to match all chosen classificators
		if(strpos($name,"classificator")===0 and strlen($parameter)>0) {
			$cid = substr($name,14);
			$rs = $db->Execute("select object from metadata m1 where xpath LIKE 'lom/classification/taxonpath/taxon/entry%' and value LIKE '$parameter'");
			foreach($rs->GetArray() as $key=>$value) $object_list.="," . $value['object']; // create a comma separated list of matched objects
		}
	}
	$list_select = (strlen($object_list)>2)? "m1.object IN ($object_list) and" : ""; // select list to refine next query
	// echo "<br>After taxonomy search: $list_select";

	// MODIFIED date search
	if(strlen($_REQUEST['from_year'])>0 && strlen($_REQUEST['from_month'])>0 && strlen($_REQUEST['from_day'])>0 &&
	strlen($_REQUEST['to_year'])>0 && strlen($_REQUEST['to_month'])>0 && strlen($_REQUEST['to_day'])>0) {
		$date_from=$_REQUEST["from_year"].'-'.$_REQUEST["from_month"].'-'.$_REQUEST["from_day"];
		$date_to=$_REQUEST["to_year"].'-'.$_REQUEST["to_month"].'-'.$_REQUEST["to_day"];
		$object_list = "-1"; // restart object list minimization
		$rs = $db->Execute("select id as object from objects m1 where $list_select modified > '$date_from' and modified<'$date_to' GROUP BY object");
		while(!$rs->EOF) {
			$object_list .= "," . $rs->fields['object'];
			$rs -> MoveNext();
		}
		$list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // select list to refine next query
	}
	// echo "<br>After modified time search: $list_select";

	// LANGUAGE search
	if(strlen($_REQUEST['lom/general/language'])>0) {
		$object_list = "-1"; // restart object list minimization
		$rs = $db->Execute("select object from metadata m1 where $list_select xpath LIKE 'lom/general/language%_" . $_REQUEST['lom/general/language'] . "' and value='on' GROUP BY object");
		while(!$rs->EOF) {
			$object_list .= "," . $rs->fields['object'];
			$rs -> MoveNext();
		}
		$list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // select list to refine next query
	}
	// echo "<br>After language search: $list_select";

	// CONTEXT search
	if(strlen($_REQUEST['lom/educational/context'])>0) {
		$object_list = "-1"; // restart object list minimization
		$rs = $db->Execute("select object from metadata m1 where $list_select xpath LIKE 'lom/educational/context%_" . $_REQUEST['lom/educational/context'] . "' and value='on' GROUP BY object");		
		while(!$rs->EOF) {
			$object_list .= "," . $rs->fields['object'];
			$rs -> MoveNext();
		}
		$list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // select list to refine next query
	}
	// echo "<br>After context search: $list_select";

	// AGERANGE search
	if($_REQUEST['agefrom']!="U" || $_REQUEST['ageto']!="U") {
		$object_list = "-1"; // restart object list minimization
		$rs = $db->Execute("select object from metadata m1 where $list_select xpath LIKE 'lom/educational/typicalagerange%' and value='" . $_REQUEST['agefrom'] . "-" . $_REQUEST['ageto'] . "' GROUP BY object");
		while(!$rs->EOF) {
			$object_list .= "," . $rs->fields['object'];
			$rs -> MoveNext();
		}
		
		$list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // select list to refine next query
	}
        // echo "<br>After age range search: $list_select";

        // FORMAT search
        if(strlen($_REQUEST['lom/technical/format'])>0) {
                $object_list = "-1"; // restart object list minimization
                $rs = $db->Execute("select object from metadata m1 where $list_select xpath LIKE 'lom/technical/format%_" . $_REQUEST['lom/technical/format'] . "' and value='on' GROUP BY object");
                while(!$rs->EOF) {
                        $object_list .= "," . $rs->fields['object'];
                        $rs -> MoveNext();
                }
                $list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // select list to refine next query
        }
        // echo "<br>After format search: $list_select";

        // PAID search
        if(strlen($_REQUEST['lom/rights/cost'])>0) {
                $object_list = "-1"; // restart object list minimization
                $rs = $db->Execute("select object from metadata m1 where $list_select xpath LIKE 'lom/rights/cost%' and value='" . $_REQUEST['lom/rights/cost'] . "' GROUP BY object");
                while(!$rs->EOF) {
                        $object_list .= "," . $rs->fields['object'];
                        $rs -> MoveNext();
                }
                $list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // $
        }
        // echo "<br>After paid search: $list_select";

	// AUTHOR search
	if(strlen($_REQUEST['lom/lifecycle/contribute/entity'])>0) {
		$object_list = "-1"; // restart object list minimization
		$rs = $db->Execute("select object from metadata m1 where $list_select xpath LIKE 'lom/lifecycle/contribute/entity%' and value='" . $_REQUEST['lom/lifecycle/contribute/entity'] . "' GROUP BY object");
		while(!$rs->EOF) {
			$object_list .= "," . $rs->fields['object'];
			$rs -> MoveNext();
		}
		$list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; 
	}
	// echo "<br>After author search: $list_select";
	
	// KEYWORD search
	if(strlen($_REQUEST['search'])>0 and strcmp($_REQUEST['search'],"Paieška:")) {
		$object_list = "-1"; // restart object list minimization
		$rs = $db->Execute("select object from metadata m1 where $list_select value LIKE '%" . $_REQUEST['search'] ."%' GROUP BY object");
		while(!$rs->EOF) {
			$object_list .= "," . $rs->fields['object'];
			$rs -> MoveNext();
		}
		$list_select = (strlen($object_list)>2)? " m1.object IN ($object_list) and" : ""; // select list to refine next query
	}
	// echo "<br>After keyword search: $list_select";

	// --------------------------------------------------------------------------------------
	// prepare final result print out
	$order_by=$_GET["order_by"];
	switch($order_by) {
			case 'modified':
				$order="modified desc";
				break;
			case 'title':
				$order="title asc";
				break;
			case 'object_rating':
				$order="object_rating desc";
				break;	
			case 'clicks':
				$order="clicks desc";
				break;	
			default:
				$order="modified desc";
				break;
		}			
	
	// final query to display objects
	$sql = "SELECT m1.object as object,m1.value as title,m2.value as subject_id,m3.value as description ,m4.value as age_range,com1 as quantity_of_comments, AVG( or1.rating ) as object_rating , COUNT( or1.rating) as rated_quantity,o1.modified as modified ,ci1.title as subject_name,c1 as clicks FROM metadata m1 
        	 LEFT JOIN metadata m2 on m1.object=m2.object
		 LEFT JOIN metadata m3 on m1.object=m3.object
		 LEFT JOIN metadata m4 on m1.object=m4.object
		 LEFT JOIN (select object_id,count(*) as com1 from commentary group by object_id)
		 as comments on m1.object = comments.object_id
		 LEFT JOIN objects o1 on m1.object=o1.id
		 LEFT JOIN objects_rating or1 ON m1.object = or1.object_id
		 LEFT JOIN classificator_items ci1 ON m2.value = ci1.id
		 LEFT JOIN (select object_id,count(*) as c1 from object_view_counter where REQUEST_URL like 'http://%' group by object_id)as counter on m1.object = counter.object_id 
		 WHERE $list_select m1.xpath LIKE 'lom/general/title%' and 
		 m2.xpath LIKE 'lom/classification/taxonpath/taxon/entry%'
		 AND m3.xpath LIKE 'lom/general/description%'
		 AND m4.xpath like 'lom/educational/typicalagerange%'
		 group by m1.object order by $order $current_limit";
	
	// echo $sql;
	
	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	$at_all=0;
	if(strlen($list_select)>0) {
		$rs = $db->Execute("$sql");
		$at_all = substr_count($list_select, ','); // count number of commas in object list
		while(!$rs->EOF) {
			$object_id = $rs->fields['object'];
		
			// echo "<br>Object $object_id ";
			$rating_rounded = $rs->fields["object_rating"];
			$quantity = $rs1->fields["COUNT(rating)"];
			$rating = round($rating_rounded, 0);
			$title_short = $rs->fields["title"];
			$title_long = $rs->fields["description"];
			$age_group = $rs->fields["age_range"];
			$subjects = $rs->fields["subject_name"];
			$quantity_com1 = $rs->fields["quantity_of_comments"];
			$quantity_com = round($quantity_com1, 0);
			$quantity1 = $rs->fields["rated_quantity"];
			$quantity = round($quantity1, 0);
			$clicks = $rs->fields["clicks"];

			// find all taxonomy subjects
			$subjects=''; $subjects2='';
			$atempt=0;
			$quantity_subjects=0;
			$rs1 = $db->Execute("select value from metadata where object=$object_id and xpath like 'lom/classification/taxonpath/taxon/entry%' and value<>''");	
			while(!$rs1->EOF) {
				$taxon_id = $rs1->fields["value"];
				// echo "<br>select id,title from classificator_items where id=$taxon_id";
				$rs2 = $db->Execute("select id,title from classificator_items where id=$taxon_id");	
				$taxon_title = $rs2->fields["title"];
				$subject_id = $rs2->fields["id"];
				$rs2 -> close();
				if($atempt==0){
					$subjects = $subjects . "<a href='objects_by_subjects.php?subject=$subject_id&order_by=modified&page=1'>$taxon_title</a>";
					$subjects2 = $subjects2 . "$taxon_title";
					$atempt=1;
				} else{
					$subjects = $subjects .' , '. "<a href='objects_by_subjects.php?subject=$subject_id&order_by=modified&page=1'>$taxon_title</a>";
					$subjects2 = $subjects2 .' , '. "$taxon_title";
				}
				$quantity_subjects++;
				$rs1 -> MoveNext();
			}
			if($quantity_subjects>0){
		$quantity=$quantity/$quantity_subjects;}
			if($clicks>0){
				$clicks="Parsisiuntimai:<img src='./img/icon_2.gif' width='10' border='0' /> ".$clicks;
			}
			
			$html = "<a href=\"JavaScript:open_win('object_view.php?object_id=$object_id', '600', '800')\"><h2>$title_short</h2></a>
			<p>$title_long</p>
		 
			<a href=\"JavaScript:open_win('object_view.php?object_id=$object_id', '600', '800')\" class='more' >Plačiau</a>
		
			</td>
			</tr> ";
			$tpl -> assign(ID, $html);
			
			$html2 = "<h2>$title_short</h2>
				<p>$title_long</p>	
				<a href='./../user/object_export_lom.php?object_id=$object_id'>Parsisiųsti objektą xml formatu</a>
				</td>
				</tr> ";
			$_SESSION["html"][$object_id]=$html2;
			$tpl -> parse(viewlist, ".view");
			$rs -> MoveNext();
		}
	} else{
		$tpl -> assign(ID, "Atsiprašome, tačiau neradome jūsų užklausą tenkinančių mokymosi objektų. Pabandykite įvesti bendresnę užklausą.");
	}
	
	$tpl -> assign(AT_ALL, $at_all);

	// page navigation buttons
if($at_all != 0){
	$n=0;
	$pages=1;
	$back_page = $page-1;
	if($back_page>0){
		$print_pages="<div id='pages'><a href='search_result.php?order_by=$order_by&page=$back_page'>Atgal</a>  | <a href='search_result.php?order_by=$order_by&page=1'>1</a>";
	}
	else{
		$print_pages="<div id='pages'><a href='search_result.php?order_by=$order_by&page=1'>1</a>";
	}
	for($i=0;$i<=$at_all;$i++){
		$n++;
		if($n==10){
			$pages++;
			$print_pages = $print_pages ." | <a href='search_result.php?order_by=$order_by&page=$pages'>$pages</a>";
			$n=0;
		}
	}
	$foward_page =  $page+1;
	if($foward_page>$pages){
		$print_pages = $print_pages ."</div>";
	} else{
		$print_pages = $print_pages ." | <a href='search_result.php?order_by=$order_by&page=$foward_page'>Toliau</a></div>";
	}
	$tpl -> assign(PRINT_PAGES, $print_pages);	
	} else{ 
$tpl -> assign(PRINT_PAGES, '');
}	
		
		
	$tpl -> parse(HEAD, "head");
	$tpl ->parse(MENIU,"menu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
