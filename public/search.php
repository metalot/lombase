<?php
	include("../include/config_public.php");
	include("../include/class.Classificator.php");
	if(strlen($header)==0) $header="header_public.html"; // check if the header is included elsewhere

	$tpl = new FastTemplate("../templates/");
	$tpl -> define( array(
		head => $header,
		main => "search_simple.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"class=\"active\"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
