﻿<?php
	include("../include/config.php");
	include("../include/xml_struct.php");

	if(!isset($_GET['object_id'])) {
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];
	$user_name=$_SESSION["user_name"];
	$user_email=$_SESSION["user_email"];
	setcookie('cookietest', 'none', time() + 86400*10);
	if(isset($_SESSION["user_name"])){
	setcookie("name", $user_name, time() + 86400*10);
	setcookie("email", $user_email, time() + 86400*10);
	unset($_SESSION["user_name"]);
	header("location: object_view_coment.php?object_id=$object_id");
	}
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);

	$tpl -> define( array(
		main => "object_view_comment.html"
	));	
	if(strlen($_SESSION["message"])>5){
	$tpl -> assign(MESSAGE, $_SESSION["message"]);
	unset($_SESSION["message"]);
	}
	else{
	$tpl -> assign(MESSAGE, '');
	}	
			
		$rs = $db->Execute("SELECT * FROM `objects` where id=$object_id");
	while(!$rs->EOF) {
	$object_id = $rs->fields['id'];
	$rs1 = $db->Execute("select avg(rating),COUNT(rating) from objects_rating where object_id=$object_id");
		$rating_rounded = $rs1->fields["avg(rating)"];
		$quantity = $rs1->fields["COUNT(rating)"];
		$rating = round($rating_rounded, 0);
		$tpl -> assign(AVG, $rating_rounded);
		$rs1 -> close();
		$rs2 = $db->Execute("select * from metadata where object=$object_id and xpath LIKE 'lom/general/title%'");
		$title_short = $rs2->fields['value'];
		$tpl -> assign(TITLE_SHORT, $rs2->fields['value']);
		$rs2 -> close();
		$rs3 = $db->Execute("select * from metadata where object=$object_id and xpath LIKE 'lom/general/description%'");
		$tpl -> assign(TITLE_LONG, $rs3->fields['value']);
		$title_long = $rs3->fields['value'];
		$rs3 -> close();
		$rs1 = $db->Execute("select COUNT(user_name) from commentary where object_id=$object_id");
		$quantity_com = $rs1->fields["COUNT(user_name)"];
			$rs1 -> close();
		$rs1 = $db->Execute("select value from metadata where object=$object_id and xpath like 'lom/educational/typicalagerange%'");	
		$age_group = $rs1->fields["value"];
			$rs1 -> close();
			$atempt=0;
		$subjects='';
		
		$rs1 = $db->Execute("select value from metadata where object=$object_id and xpath like 'lom/classification/taxonpath/taxon/entry%'");	
		while(!$rs1->EOF) {
			$taxon_id = $rs1->fields["value"];
			if(strlen($taxon_id)>0){
	$rs2 = $db->Execute("select id,title from classificator_items where id=$taxon_id");	
			$taxon_title = $rs2->fields["title"];
			$subject_id = $rs2->fields["id"];
			$rs2 -> close();}
			if($atempt==0){
			$subjects = $subjects . "<a href='objects_by_subjects.php?subject=$subject_id&order_by=modified&page=1'>$taxon_title</a>";
			$atempt=1;
			}
			else{
			$subjects = $subjects .' | '. "<a href='objects_by_subjects.php?subject=$subject_id&order_by=modified&page=1'>$taxon_title</a>";
			}
			
		$rs1 -> MoveNext();
		}
		
			$rs1 -> close();
		switch($rating) {
			case 1:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 2:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 3:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 4:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 5:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' />";
				break;
			default:
				$stars="<img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
		}		
	
		$rs1 = $db->Execute("select count(id) from object_view_counter where object_id=$object_id and REQUEST_URL like 'http://%'");	
		$clicks = $rs1->fields["count(id)"];
		if($clicks>0){
		$clicks="Parsisiuntimai:<img src='./img/icon_2.gif' width='10' border='0' /> ".$clicks;
		}
		else{
		$clicks='';
		}
/*$html = "<a href=' ' onclick=\"popitup2('object_view.php?object_id=$object_id')\"><h2>$title_short</h2></a>
		<p>$title_long</p>
	
		<div class='row_1'>Amžius $age_group <span>$subject </span></div> 
		$stars ($quantity)  $clicks <a href=' ' onclick=\"popitup('object_view_coment.php?object_id=$object_id')\"> Vertinimai/Komentarai($quantity_com) </a> <img src='./img/icon_3.gif' width='4' border='0' /> <br> 
		<a href=' ' onclick=\"popitup2('object_view.php?object_id=$object_id')\" class='more' >Plačiau</a>
		<a href='./../user/object_export_lom.php?object_id=$object_id'>Parsisiųsti objektą xml formatu</a>
		</td>
</tr> ";*/
		$html = "<h2>$title_short</h2>
		<p>$title_long</p>
	
		<div class='row_1'>Amžius $age_group <span>$subject </span></div> 
		$stars ($quantity)  $clicks <br> 
	
		</td>
</tr> ";

		$tpl -> assign(AVG, $html);
		$at_all++;
	$rs -> MoveNext();
	}
	if(isset($_GET["vote"])){
	$voted=$_GET["vote"];
	$ranked =$_GET["rank"];
		if($voted==0){
		$vote_info = 'Jūs įvertinote: '.$ranked;
		}
		else{
		$vote_info = 'Jūs jau esate įvertinęs šį objektą!';
		}
		$tpl->assign(VOTED,$vote_info);
	}
	else{
	$tpl->assign(VOTED,'');
	}
	
	
	
	$tpl->assign(TITLE,"LOMBASE " . $version);

	$rand_min = rand(1, 9);
	 $rand_max = rand(1, 9);
	 
	$tpl->assign(RAND_MIN, $rand_min);
	$tpl->assign(RAND_MAX, $rand_max);
	$tpl->assign(ID, $object_id);
	if((isset($_COOKIE['name']))&&(isset($_COOKIE['email']))){
	$vardas=$_COOKIE['name'];
	$emailas= $_COOKIE['email'];
	if(isset($_SESSION["com_text"])){
	$komentaras= $_SESSION["com_text"];}
	else{
	$komentaras='';}
	$tpl->assign(VARDAS, $vardas);
	$tpl->assign(EMAILAS, $emailas);
	$tpl->assign(KOMENTARAS, $komentaras);
	}
	
	else{
		$tpl->assign(VARDAS,'');
	$tpl->assign(EMAILAS, '');
	$tpl->assign(KOMENTARAS, '');
	
	}
	if(!isset($_COOKIE['voted'.$object_id])){
	$rating_add="   <td width='170'><h2 align='right'>Įvertinti objektą: </h2></td>
            <td width='303'><table width='104' height='49'></td>
              </tr>
                <td width='13'><a href='./add_rating.php?object_id=$object_id&amp;rank=1'><img src='./img/star_1.gif' alt='a' width='12' height='12' border='0' /></a></td>
                <td width='13'><a href='./add_rating.php?object_id=$object_id&amp;rank=2'><img src='./img/star_1.gif' alt='a' width='12' height='12' border='0' /></a></td>
                <td width='13'><a href='./add_rating.php?object_id=$object_id&amp;rank=3'><img src='./img/star_1.gif' alt='a' width='12' height='12' border='0' /></a></td>
                <td width='14'><a href='./add_rating.php?object_id=$object_id&amp;rank=4'><img src='./img/star_1.gif' alt='a' width='12' height='12' border='0' /></a></td>
                <td width='12'><a href='./add_rating.php?object_id=$object_id&amp;rank=5'><img src='./img/star_1.gif' alt='a' width='12' height='12' border='0' /></a></td>
                <td width='11'>&nbsp;</td>
              </td></tr>
              <tr>
                <td><div align='center'>1</div></td>
                <td><div align='center'>2</div></td>
                <td><div align='center'>3</div></td>
                <td><div align='center'>4</div></td>
                <td><div align='center'>5</div></td>
                <td><label></label></td>
              </tr>
            </table></td>";
			$tpl->assign(RATING_ADD, $rating_add);
	}
	else{
	$rating_add="Jūs balsavote: ".$_COOKIE['voted'.$object_id];
	$tpl->assign(RATING_ADD, $rating_add);
	
	}
	
	$tpl -> define_dynamic("view", "main");
	$count=0;
	$db = dbc();
	$rs = $db->Execute("select com_id,user_name, com_text, date,rate,need_check from commentary where $object_id=object_id order by time desc");
	  while(!$rs->EOF) {
				$count++;
				$user_auth = $rs->fields['user_name'];
				$tpl -> assign(COM_TEXT, $rs->fields['com_text']);				
				$tpl -> assign(DATE, '('.$rs->fields['date'].')');
				$com_id= $rs->fields['com_id'];
				$tpl -> assign(COM_ID, $rs->fields['com_id']);
				$tpl -> assign(USER_AUTH, $user_auth);
				$rating = $rs->fields['rate'];
				switch($rating) {
			case 1:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 2:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 3:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 4:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 5:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' />";
				break;
			default:
				$stars="<img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
		}		
		if($rs->fields['need_check']!=1){
		$notice="<a title='Delete object' href='object_com_need_check.php?id=$com_id');> Pranešti apie netinkamą komentarą</a><img src='./img/icon_3.gif' width='4' border='0' />";
		$tpl ->assign(NOTICE, $notice);
		}	
		else{
		
		//$notice="<p>Netinkamas komentaras</p>";
		$notice="<a title='Delete object' href='object_com_need_check.php?id=$com_id');> Pranešti apie netinkamą komentarą</a><img src='./img/icon_3.gif' width='4' border='0' />";
		$tpl ->assign(NOTICE, $notice);
		}	
		$tpl ->assign(STARS, $stars);		
		$tpl -> parse(viewlist, ".view");		
		$rs -> MoveNext();		
				}
	if($count==0){
				$tpl -> assign(COM_TEXT, '');				
				$tpl -> assign(DATE, '');
				$tpl -> assign(USER_AUTH, 'Objektas komentarų neturi');
				$tpl ->assign(STARS, '');
				$tpl ->assign(NOTICE, '');
	}	
	 
	
	
	
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>
