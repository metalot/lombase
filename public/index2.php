<?php
	//header('Location: ../../admin/login.php');
	
	error_reporting(E_ALL ^ E_NOTICE);

	include("../include/class.FastTemplate.php"); // Fast Template library
	include("../include/lang_lt.php"); // language file
	$tpl = new FastTemplate("../templates/");
	
	$tpl -> define( array(
		head => "header_public.html",
		main => "login.html",
		footer => "footer.html"
	));
	
	$msg = ($_GET['login'] === "failed" ? lang('login_failed') : "");

	$tpl -> assign(TITLE,"Mokomųjų objektų saugykla");
	$tpl -> assign(ACTIVE_MAIN,"class=\"active\"");
	$tpl -> assign(MSG, $msg);
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>
