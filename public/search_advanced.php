<?php
	include("../include/config_public.php");
	include("../include/class.Classificator.php");
	if(strlen($header)==0) $header="public_header.html"; // check if the header is included elsewhere

	$tpl = new FastTemplate("../templates/");
	$tpl -> define( array(
		head => $header,
		main => "search.html",
		menu=> "menu.html",
		footer => "public_footer.html"
	));

	$tpl -> define_dynamic("classificator", "main");
	
	$db = dbc();
	$rs = $db->Execute("select * from classificators where searchable=1");
	$classificator_db = $rs->GetArray();
	for($i=0;$i<count($classificator_db); $i++) {
		$classificator = new Classificator($db, $classificator_db[$i]['id']); 
		$classificator->createForm();
		$tpl -> assign(CLASSIFICATOR_ID, $classificator_db[$i]['id']);
		$tpl -> assign(CLASSIFICATOR_TITLE, $classificator_db[$i]['title']);
		$tpl -> assign(CLASSIFICATOR_OUTPUT, $classificator->form);
		$tpl -> parse(classificatorlist, ".classificator");
	}
	
	// languages
	$languages="<select name=\"lom/general/language\"><option></option>";
	$form_values = split(",,", $db->GetOne("select value_lt from meta1 where xpath='lom/general/language'"));
	foreach($form_values as $form_value) {
		// Separate value from user friendly display
		if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
			$languages.="<option value=\"$matches[1]\">$matches[2]</option>";
		}
	}

	// format
	$format="<select name=\"lom/technical/format\"><option></option>";
	$form_values = split(",,", $db->GetOne("select value_lt from meta1 where xpath='lom/technical/format'"));
	for($i=0;$i<count($form_values); $i++) {
		$form_id = $i+1;
		$format.="<option value=\"$form_id\">$form_values[$i]</option>";
	}

	// type
	$type="<select name=\"lom/educational/learningresourcetype\"><option></option>";
	$form_values = split(",,", $db->GetOne("select value_lt from meta1 where xpath='lom/educational/learningresourcetype'"));
	foreach($form_values as $form_value) {
		$type.="<option>$form_value</option>";
	}
	
	// context
	$context="<select name=\"lom/educational/context\"><option></option>";
	$form_values = split(",,", $db->GetOne("select value_lt from meta1 where xpath='lom/educational/context'"));
	for($i=0;$i<count($form_values); $i++) {
		$form_id = $i+1;
		$context.="<option value=\"$form_id\">$form_values[$i]</option>";
	}
	
	// authors
	$author="<input name=\"lom/lifecycle/contribute/entity\">";
	
	$tpl->assign(LOLANGUAGE, $languages . "</select>");
	$tpl->assign(LOFORMAT, $format . "</select>");
	$tpl->assign(LOTYPE, $type . "</select>");
	$tpl->assign(LOCONTEXT, $context . "</select>");
	$tpl->assign(LOAUTHOR, $author . "</select>");
	
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"class=\"active\"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl ->parse(MENIU,"menu");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
