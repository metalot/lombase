﻿<?php
	include_once("../include/config.php");//config file
	$subject_id=$_GET["subject"];
	$order_by=$_GET["order_by"];
	$page = $_GET["page"];
	$tpl = new FastTemplate("../templates/");
	$limit_max = $page*10;
	$limit_min = $limit_max - 10;
	
	$current_limit  = " LIMIT $limit_min".','."$limit_max ";
	
	$tpl -> define( array(
		head => "public_header.html",
		main => "objects_by_subjects.html",
		menu=> "menu.html",
		footer => "public_footer.html"
	));
	
	//$msg = ($_GET['login'] === "failed" ? lang('login_failed') : "");
	
	$tpl -> assign(TITLE,"Mokomuju objektu saugykla");
	
	switch($order_by) {
			case 'modified':
				$order="modified desc";
				break;
			case 'title':
				$order="title asc";
				break;
			case 'object_rating':
				$order="object_rating desc";
				break;	
			case 'clicks':
				$order="clicks desc";
				break;	
			default:
				$order="modified desc";
				break;
		}			
			
		$sql = 'SELECT m1.object as object_id,m1.value as title,m2.value as subject_id,m3.value as description ,m4.value as age_range,com1 as quantity_of_comments, AVG( or1.rating ) as object_rating , COUNT( or1.rating) as rated_quantity,o1.modified as modified ,ci1.title as subject_name,c1 as clicks FROM metadata m1 '
        . ' LEFT JOIN metadata m2 on m1.object=m2.object'
        . ' LEFT JOIN metadata m3 on m1.object=m3.object'
        . ' LEFT JOIN metadata m4 on m1.object=m4.object'
        . ' LEFT JOIN (select object_id,count(*) as com1 from commentary group by object_id)'
        . ' as comments on m1.object = comments.object_id '
        . ' LEFT JOIN objects o1 on m1.object=o1.id'
        . ' LEFT JOIN objects_rating or1 ON m1.object = or1.object_id'
        . ' LEFT JOIN classificator_items ci1 ON m2.value = ci1.id'
        . ' Left join (select object_id,count(*) as c1 from object_view_counter where REQUEST_URL like \'http://%\' group by object_id)as counter on m1.object = counter.object_id '
        . ' WHERE m1.xpath LIKE \'lom/general/title%\' '
        . ' AND m2.xpath LIKE \'lom/classification/taxonpath/taxon/entry%\' and m2.value='.$subject_id
        . ' AND m3.xpath LIKE \'lom/general/description%\''
        . ' AND m4.xpath like \'lom/educational/typicalagerange%\''
        . ' group by m1.object order by '.$order . $current_limit
        . ' ';
	
	
	
	
	
	$tpl -> define_dynamic("view", "main");
	$db = dbc();
	$rs = $db->Execute("$sql");
	while(!$rs->EOF) {
	$object_id = $rs->fields['object_id'];
		
		$rating_rounded = $rs->fields["object_rating"];
		$quantity = $rs1->fields["COUNT(rating)"];
		$rating = round($rating_rounded, 0);
	$title_short = $rs->fields["title"];
	$title_long = $rs->fields["description"];
	$age_group = $rs->fields["age_range"];
	$subjects = $rs->fields["subject_name"];
	$quantity_com1 = $rs->fields["quantity_of_comments"];
	$quantity_com = round($quantity_com1, 0);
	$quantity1 = $rs->fields["rated_quantity"];
	$quantity = round($quantity1, 0);
		switch($rating) {
			case 1:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 2:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 3:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 4:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 5:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' />";
				break;
			default:
				$stars="<img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
		}		
		
		$clicks = $rs->fields["clicks"];
		$atempt=0;
		$subject='';
		$rs1 = $db->Execute("select value from metadata where object=$object_id and xpath like 'lom/classification/taxonpath/taxon/entry%'");	
		while(!$rs1->EOF) {
			$taxon_id = $rs1->fields["value"];
			$rs2 = $db->Execute("select id,title from classificator_items where id=$taxon_id");	
			$taxon_title = $rs2->fields["title"];
			$subject_id2 = $rs2->fields["id"];
			if($subject_id==$subject_id2){
			$tpl -> assign(CURRENT_SUBEJT, $taxon_title);
			}
					
			if($atempt==0){
			$subject = $subject . "<a href='objects_by_subjects.php?subject=$subject_id2&order_by=modified&page=1'>$taxon_title</a>";
			$atempt=1;
			}
			else{
			$subject = $subject .','. "<a href='objects_by_subjects.php?subject=$subject_id2&order_by=modified&page=1'>$taxon_title</a>";
			}
		$rs1 -> MoveNext();
		}
		if($clicks>0){
		$clicks="Parsisiuntimai:<img src='./img/icon_2.gif' width='10' border='0' /> ".$clicks;
		}
			else{
		$clicks='';
		}
		$html = "<a href=\"JavaScript:open_win('object_view.php?object_id=$object_id', '600', '800')\"><h2>$title_short</h2></a>
		<p>$title_long</p>
	
		<div class='row_1'>Amžius $age_group <span>$subject </span></div> 
		$stars ($quantity)  $clicks <a href=\"JavaScript:open_win('object_view_coment.php?object_id=$object_id', '600', '800')\"> Vertinimai/Komentarai($quantity_com) </a> <img src='./img/icon_3.gif' width='4' border='0' /> <br> 
		<a href=\"JavaScript:open_win('object_view.php?object_id=$object_id', '600', '800')\" class='more' >Plačiau</a>
		
		</td>
</tr> ";
			$tpl -> assign(ID, $html);
		
		$subjects='';
		$rs1 = $db->Execute("select value from metadata where object=$object_id and xpath like 'lom/classification/taxonpath/taxon/entry%'");	
		while(!$rs1->EOF) {
			$taxon_id = $rs1->fields["value"];
/*			$rs2 = $db->Execute("select id,title from classificator_items where id=$taxon_id");	
			$taxon_title = $rs2->fields["title"];
			$subjects_id = $rs2->fields["id"];
			$rs2 -> close();
*/			if($atempt==0){
			$subjects = $subjects . "$taxon_title";
			$atempt=1;
			}
			else{
			$subjects = $subjects .' | '. "$taxon_title";
			}
		$rs1 -> MoveNext();
		}
		$html2 = "<h2>$title_short</h2>
		<p>$title_long</p>	
		<div class='row_1'>Amžius $age_group $subjects </div> 
		$stars ($quantity) $clicks Vertinimai/Komentarai($quantity_com) <img src='./img/icon_3.gif' width='4' border='0' /><br> 
	<a href='./../user/object_export_lom.php?object_id=$object_id'>Parsisiųsti objektą xml formatu</a>
		</td>
</tr> ";
	$_SESSION["html"][$object_id]=$html2;
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
	if(strlen($html)<5){
		$tpl -> assign(ID, "Rezultatų nerasta!");
		
		}
	
	$db = dbc();
	$rs1 = $db->Execute("select count(item) from metadata where xpath LIKE 'lom/classification/taxonpath/taxon/entry%' and value=$subject_id");
	$at_all = $rs1->fields["count(item)"];
	$tpl -> assign(AT_ALL, $at_all);
	//------------------------------------------------------
	$n=0;
	$pages=1;
	$back_page = $page-1;
	if($back_page>0){
	$print_pages="<div id='pages'><a href='objects_by_subjects.php?subject=$subject_id&order_by=$order_by&page=$back_page'>Atgal</a>  | <a href='objects_by_subjects.php?subject=$subject_id&order_by=$order_by&page=1'>1</a>";
	}
	else{
	$print_pages="<div id='pages'><a href='objects_by_subjects.php?subject=$subject_id&order_by=$order_by&page=1'>1</a>";
	}
	for($i=0;$i<=$at_all;$i++){
	$n++;
		if($n==10){
		$pages++;
		$print_pages = $print_pages ." | <a href='objects_by_subjects.php?subject=$subject_id&order_by=$order_by&page=$pages'>$pages</a>";
		$n=0;
		}
	}
	$foward_page =  $page+1;
	if($foward_page>$pages){
	$print_pages = $print_pages ."</div>";
	}
	else{
	$print_pages = $print_pages ." | <a href='objects_by_subjects.php?subject=$subject_id&order_by=$order_by&page=$foward_page'>Toliau</a></div>";
	}
	$tpl -> assign(PRINT_PAGES, $print_pages);
	$tpl -> assign(SUBJECT_ID, $subject_id);
	$tpl -> parse(HEAD, "head");
	$tpl ->parse(MENIU,"menu");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
	
?>	
