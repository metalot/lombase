<?php
	$object_id = $_GET['object_id'];
	$_SESSION["object_id"] = $object_id;
	include("../include/config.php");
	include("../include/xml_struct_public.php");

	if(!isset($_GET['object_id'])) {
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);

	$tpl -> define( array(
		main => "object_view_public.html"
	));	
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"class=\"active\"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$db = dbc();
	$rs = $db->Execute("select * from objects where id=$object_id");
	$fileLink = ""; 
	if(strlen($rs->fields['file_name'])>0) {
			$fileLink = "<a href=\"object_showfile.php?object_id=$object_id\">" .  $rs->fields['file_name'] . "</a>";
	}
	$tpl -> assign(FILELINK, $fileLink);

	$rs1 = $db->Execute("select avg(rating),COUNT(rating) from objects_rating where object_id=$object_id");
		$rating = $rs1->fields["avg(rating)"];
		
		$rating_rounded = round($rating, 0);
		$quantity = $rs1->fields["COUNT(rating)"];
		switch($rating) {
			case 1:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 2:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 3:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 4:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
			case 5:
				$stars="<img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' /> <img src='./img/star_21gif' width='12' height='12' border='0' /> <img src='./img/star_1.gif' width='12' height='12' border='0' />";
				break;
			default:
				$stars="<img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' /> <img src='./img/star_2.gif' width='12' height='12' border='0' />";
				break;
		}		
		$html = $_SESSION["html"][$object_id];
		$html = "<a href='./../user/object_export_lom.php?object_id=$object_id'>Parsisiusti mokymosi objekto metaduomenis XML formatu</a>";
			$tpl -> assign(AVG, $html);


	// preloading object
	// the lom root id 0
	unset($_SESSION['xml_instances']);
	$_SESSION['xml_instances']["lom0"] = array (
				'id' => 0,
				'key' => "lom0",
				'value' => "LRE LOM",
				'parent' => 0					
				);				

	$rs = $db->Execute("select * from metadata where object=$object_id");
	while(!$rs->EOF) {
		$value = $rs->fields['value'];
		if(preg_match("!lom/technical/location!",$rs->fields['xpath']) && strlen($fileLink)>0) {
			$value = $fileLink;
		}

		$_SESSION['xml_instances'][$rs->fields['xpath']] = array (
					'id' => $rs->fields['item'],
					'key' => $rs->fields['xpath'],
					'value' => $value,
					'parent' => $rs->fields['parent']					
					);		
		$rs -> MoveNext();
	}

	// load form metadata
	$rs = $db->Execute("select * from meta1");
	$xml_db = $rs->GetArray();
	$xmlForm = new XmlInputForm($xml_db);
	$xmlForm->mode=0; // viewing mode
//	$xmlForm->debug=1; // debug mode
	$xmlForm->initXmlInstancesFromSession("lom"); // initialize object from session
	$xmlForm->generateXmlForm("lom"); // generate form
	$form = $xmlForm->form;
	$rand_min = rand(1, 9);
	 $rand_max = rand(1, 9);
	$tpl->assign(RAND_MIN, $rand_min);
	$tpl->assign(RAND_MAX, $rand_max);
	$tpl->assign(VIEW_COM, "./object_view_com.php?object_id=$object_id");			
	$tpl->assign(ID, $object_id);
	$tpl->assign(OBJECT,$form);
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>
