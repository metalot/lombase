<?php
	include("../include/config.php");
	if(!isset($_GET['object_id'])){
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];
	$grp = $_SESSION['grp'];
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	
	if($grp=='a'){
	$tpl -> define( array(
		head => "header.html",
		main => "object_view_com_a.html",
		menu=> "menu.html",
		footer => "footer.html"
	));
	}
	else{
	$tpl -> define( array(
		head => "public_header.html",
		main => "object_view_com.html",
		menu=> "menu.html",
		footer => "public_footer.html"
	));	}
	$tpl->assign(TITLE,"LOMBASE " . $version);

	$tpl -> define_dynamic("view", "main");
	$count=0;
	$db = dbc();
	$rs = $db->Execute("select com_id,user_name, com_text, date from commentary where $object_id=object_id order by date desc");
	  while(!$rs->EOF) {
				$count++;
				$user_auth = $rs->fields['user_name'];
				$tpl -> assign(COM_TEXT, $rs->fields['com_text']);				
				$tpl -> assign(DATE, $rs->fields['date']);
				$tpl -> assign(ID, $rs->fields['com_id']);
				$tpl -> assign(USER_AUTH, $user_auth);
		$tpl -> parse(viewlist, ".view");		
		$rs -> MoveNext();		
				}
	if($count==0){
				$tpl -> assign(COM_TEXT, 'neturi!');				
				$tpl -> assign(DATE, 'objektas');
				$tpl -> assign(USER_AUTH, 'Komentaru');
	}	

	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl ->parse(MENIU,"menu");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>
