<?php
	include("../include/config.php");
	include("../include/xml_struct.php");

	if(!isset($_GET['object_id'])) {
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);

	$tpl -> define( array(
		head => "header_public.html",
		main => "object_view.html",
		footer => "footer.html"
	));	
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"class=\"active\"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$db = dbc();
	$rs = $db->Execute("select * from objects where id=$object_id");
	$fileLink = ""; 
	if(strlen($rs->fields['file_name'])>0) {
			$fileLink = "<a href=\"object_showfile.php?object_id=$object_id\">" .  $rs->fields['file_name'] . "</a>";
	}
	$tpl -> assign(FILELINK, $fileLink);

	$rs1 = $db->Execute("select avg(rating) from objects_rating where object_id=$object_id");
		$rating = $rs1->fields["avg(rating)"];
		$rating_rounded = round($rating, 2);
		$tpl -> assign(AVG, $rating_rounded);


	// preloading object
	// the lom root id 0
	unset($_SESSION['xml_instances']);
	$_SESSION['xml_instances']["lom0"] = array (
				'id' => 0,
				'key' => "lom0",
				'value' => "LRE LOM",
				'parent' => 0					
				);				

	$rs = $db->Execute("select * from metadata where object=$object_id");
	while(!$rs->EOF) {
		$value = $rs->fields['value'];
		if(preg_match("!lom/technical/location!",$rs->fields['xpath']) && strlen($fileLink)>0) {
			
			$value = $fileLink;
		}
		// core fields like in EUN search result http://calibrate.eun.org/merlin/resources/dsp_lom.cfm?location=eun___15998.xml
		if(preg_match("!lom/general/keyword!",$rs->fields['xpath'])) { $object['keywords'] = $rs->fields['value']; }
		if(preg_match("!lom/general/title!",$rs->fields['xpath'])) { $object['title'] = $rs->fields['value']; }
		if(preg_match("!lom/general/language!",$rs->fields['xpath'])) { $object['language'] = $rs->fields['value']; }
		if(preg_match("!lom/general/description!",$rs->fields['xpath'])) { $object['description'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }

		if(preg_match("!contribute!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		if(preg_match("!lom/general/structure!",$rs->fields['xpath'])) { $object['structure'] = $rs->fields['value']; }
		
		$rs -> MoveNext();
	}

	// load form metadata

	$tpl->assign(OBJECT,$form);
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>
