<?php
	include("../include/config_user.php");
	include("../include/xml_struct.php");

	if(!isset($_GET['object_id'])) {
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];
	$user_id = $_SESSION['user_id'];
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	if ($_SESSION['grp']=="a") {
		$tpl -> define( array(
			head => "header_admin.html",
			main => "object_view.html",
			footer => "footer.html"
			));
		$tpl->assign(TITLE,"LOMBASE " . $version);
		$tpl->assign(ACTIVE_1,"");
		$tpl->assign(ACTIVE_2,"class=\"active\"");
		$tpl->assign(ACTIVE_3,"");
		$tpl->assign(ACTIVE_4,"");
		$tpl->assign(ACTIVE_5,"");
		$tpl->assign(ACTIVE_6,"");
		$tpl->assign(ACTIVE_7,"");
	} else {
		$tpl -> define( array(
			head => "header.html",
			main => "object_view.html",
			footer => "footer.html"
		));	
		$tpl->assign(TITLE,"LOMBASE " . $version);
		$tpl->assign(ACTIVE_1,"class=\"active\"");
		$tpl->assign(ACTIVE_2,"");
		$tpl->assign(ACTIVE_3,"");
		$tpl->assign(ACTIVE_4,"");
		$tpl->assign(ACTIVE_5,"");
		$tpl->assign(ACTIVE_6,"");
		$tpl->assign(ACTIVE_7,"");
	}
	$db = dbc();	
	$rs1 = $db->Execute("select avg(rating) from objects_rating where object_id=$object_id");
		$rating = $rs1->fields["avg(rating)"];
		$rating_rounded = round($rating, 2);
		$tpl -> assign(AVG, $rating_rounded);


	$db = dbc();
	$rs = $db->Execute("select * from objects where id=$object_id");
	$fileLink = ""; 
	if(strlen($rs->fields['file_name'])>0) {
			$fileLink = "<a href=\"object_showfile.php?object_id=$object_id\">" .  $rs->fields['file_name'] . "</a>";
	}
	$tpl -> assign(FILELINK, $fileLink);

	// preloading object
	// the lom root id 0
	unset($_SESSION['xml_instances']);
	$_SESSION['xml_instances']["lom0"] = array (
				'id' => 0,
				'key' => "lom0",
				'value' => "LRE LOM",
				'parent' => 0					
				);				

	$rs = $db->Execute("select * from metadata where object=$object_id");
	while(!$rs->EOF) {
		$value = $rs->fields['value'];
		if(preg_match("!lom/technical/location!",$rs->fields['xpath']) && strlen($fileLink)>0) {
			$value = $fileLink;
		}

		$_SESSION['xml_instances'][$rs->fields['xpath']] = array (
					'id' => $rs->fields['item'],
					'key' => $rs->fields['xpath'],
					'value' => $value,
					'parent' => $rs->fields['parent']					
					);		
		$rs -> MoveNext();
	}

	// load form metadata
	$rs = $db->Execute("select * from meta1");
	$xml_db = $rs->GetArray();
	$xmlForm = new XmlInputForm($xml_db);
	$xmlForm->mode=0; // viewing mode
//	$xmlForm->debug=1; // debug mode
	$xmlForm->initXmlInstancesFromSession("lom"); // initialize object from session
	$xmlForm->generateXmlForm("lom"); // generate form
	$form = $xmlForm->form;
/*
	$rs = $db->Execute("select * from metadata where object=".$_GET['object_id']);
	while(!$rs->EOF) {
		// do not show empty fields. If field is location and there is a file attached, show link to the file
		if(strlen($rs->fields['value'])>0 || (preg_match("!lom/technical/location!",$rs->fields['xpath']) && strlen($fileLink)>0)) {
			preg_match("/(\D*)\d+_*(\w*)/",$rs->fields['xpath'],$match);
			$rs2 = $db->Execute("select * from meta1 where xpath=\"" . $match[1] . "\"");
			// Valid login
			$tpl -> assign(XMLID, 	$rs2->fields['id'] . " " . $match[1]);
			$tpl -> assign(XMLEN, 	$rs2->fields['name_en']);
			$tpl -> assign(XMLLT, 	$rs2->fields['name_lt']);
			if(strlen($match[2])>0) { // $rs2->fields['html_type'] == "CHECKBOX"
				$form_values = split(",,",$rs2->fields['value_lt']);
				$i=0;
				foreach($form_values as $form_value) {
					$i++;
					if(preg_match("/(\S+):::(\S+)/",$form_value,$matches) ) {
						$key=$matches[1];
						$display=$matches[2];
					} else {
						$key=$i;
						$display=$form_value;
					}
					if($key == $match[2]) break;
				}
				$value=$display . " (" . $match[2] . ")";
			} else {
				$value = $rs->fields['value'];
			}
			if(preg_match("!^http!",$value)) {
				$value = "<a href='$value' target=_blank>" . $value . "</a>";
			}
			if(preg_match("!lom/technical/location!",$rs->fields['xpath'])  && strlen($fileLink)>0) {
				$value = $fileLink;
			}
			$tpl -> assign(VALUE, $value);
			$tpl -> parse(viewlist, ".view");
		}
		$rs -> MoveNext();
	}
*/
	
	 $rand_min = rand(1, 9);
	 $rand_max = rand(1, 9);
	$tpl->assign(RAND_MIN, $rand_min);
	$tpl->assign(RAND_MAX, $rand_max);
	$tpl->assign(VIEW_COM, "./object_view_com.php?object_id=$object_id");			
	$tpl->assign(ID, $object_id);
	$tpl->assign(OBJECT,$form);
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;


?>
