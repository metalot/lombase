<?php
	include("../include/config_user.php");
	include("../include/xml_struct.php");
	include("../include/class.Course.php");
	include("../include/class.Classificator.php");
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);

	$tpl -> define( array(
		head => "header_public.html",
		main => "course.html",
		footer => "footer.html"
		));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"class=\"active\"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");


        $course = new Course(29);
        $course->displayForm(0,1); // displayForm(edit, version)

        $tpl->assign(FORM, $form);
	$tpl->assign(ID, $object_id);
	$tpl->assign(OBJECT,$form);
	$tpl->parse(HEAD, "head");
	$tpl->parse(FOOTER, "footer");
	$tpl->parse(MAIN, "main");
	$tpl->FastPrint(MAIN);
	exit;


?>
