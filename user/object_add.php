<?php
	include("../include/config_user.php");

	unset($_SESSION['xml_instances']);
	unset($_SESSION['object_id']);
	
        $_SESSION['language'] = $CFG->default_metadata_language;
	$_SESSION['xml_instances']['author'] = $_SESSION['user_id'];
	
	header("Location: object_edit.php?xpath=lom/general");
?>
