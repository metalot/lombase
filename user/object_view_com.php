<?php
	include("../include/config.php");
	if((!isset($_GET['object_id'])) || ($_SESSION['user_id']<0)) {
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];
	$grp = $_SESSION['grp'];
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	if($grp=='u'){
	$tpl -> define( array(
		head => "header.html",
		main => "object_view_com.html",
		footer => "footer.html"
	));	}
	if($grp=='a'){
	$tpl -> define( array(
		head => "header.html",
		main => "object_view_com_a.html",
		footer => "footer.html"
	));
	}
	$tpl->assign(TITLE,"LOMBASE " . $version);

	$tpl -> define_dynamic("view", "main");
	$count=0;
	$db = dbc();
	$rs = $db->Execute("select com_id,user_id, com_text, date from commentary where $object_id=object_id order by date desc");
	  while(!$rs->EOF) {
				$count++;
				$user_auth = $rs->fields['user_id'];
				$tpl -> assign(COM_TEXT, $rs->fields['com_text']);				
				$tpl -> assign(DATE, $rs->fields['date']);
				$tpl -> assign(ID, $rs->fields['com_id']);
				$rs1 = $db->Execute("select name, surname from users where $user_auth=id");
				$tpl -> assign(USER_AUTH, $rs1->fields['name'].' '.$rs1->fields['surname']);
		$tpl -> parse(viewlist, ".view");		
		$rs -> MoveNext();		
				}
	if($count==0){
				$tpl -> assign(COM_TEXT, 'neturi!');				
				$tpl -> assign(DATE, 'objektas');
				$tpl -> assign(USER_AUTH, 'Komentaru');
	}	

	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;

?>
