<?php
	include("../include/config_user.php"); // Fast Template library
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);	
	$tpl -> define( array(
		head => "header_simple.html",
		main => "help.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"User help for LOMBASE " . $version);
	$tpl->assign(CONTENTS,"");
	if(strlen($_REQUEST['xpath'])>0) {
			$db = dbc();
	// reikia optimizuoti - isrenkant tik top lygi
			$rs = $db->Execute("select * from meta1 where xpath like '".$_REQUEST['xpath']."'");
			
			$tpl->assign(HEADER, $rs->fields['id'] . " " . $rs->fields['xpath']);
			$tpl->assign(NAME_LT, $rs->fields['name_lt']);
			$tpl->assign(NAME_EN, $rs->fields['name_en']);
			$tpl->assign(DEFINITION_LT, $rs->fields['definition_lt']);
			$tpl->assign(DEFINITION2_LT, preg_replace("/,,/", "<br><br>", $rs->fields['definition2_lt']));
			$tpl->assign(SAMPLE_LT, $rs->fields['sample_lt']);
			$tpl->assign(LIMITS, $rs->fields['limits']);
			$tpl->assign(NOTE_LT, $rs->fields['note_lt']);
	}
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
