<?php
	include("../include/config_user.php");	

	if(strlen($_GET['object_id'])>0) {
		$db = dbc();
		$rs = $db->Execute("select * from objects where id=".$_GET['object_id']);
		if(strlen($rs->fields['file_name'])>0) {
			$_SESSION['xml_instances']['uploadedfile']['type']=$rs->fields['file_type'];
			$_SESSION['xml_instances']['uploadedfile']['size']=$rs->fields['file_size'];
			$_SESSION['xml_instances']['uploadedfile']['name']=$rs->fields['file_name'];
			$_SESSION['xml_instances']['uploadedfile']['path']=$rs->fields['file_path'];	
		}
	}

	header('Pragma: private');
	header('Cache-control: private, must-revalidate');
        header("Content-Type: " . $_SESSION['xml_instances']['uploadedfile']['type']);
        header("Content-Length: " . $_SESSION['xml_instances']['uploadedfile']['size']);
        header('Content-Disposition: attachment; filename="' . stripslashes($_SESSION['xml_instances']['uploadedfile']['name']) . '"');
	header("Content-Transfer-Encoding: binary\n");
        echo fread(fopen($_SESSION['xml_instances']['uploadedfile']['path'], "r"), filesize($_SESSION['xml_instances']['uploadedfile']['path']));
?>
