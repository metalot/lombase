<?php
	include("../include/config_user.php");

	/*
		ALTER TABLE objects ADD `file_name` varchar( 255 ) COLLATE latin1_bin NOT NULL default '',
		ADD `file_type` varchar( 255 ) COLLATE latin1_bin NOT NULL default '',
		ADD `file_size` int( 10 ) unsigned NOT NULL default '0',
		ADD `file_data` mediumblob NOT NULL 
	*/
	// save file

	if ($_FILES['uploadedfile']['size']>0) {
		
		$path = "../files/userid_" . $_SESSION['user_id'];
		$_SESSION['xml_instances']['uploadedfile']['name'] = addslashes($_FILES['uploadedfile']['name']);
		$_SESSION['xml_instances']['uploadedfile']['type'] = $_FILES['uploadedfile']['type'];
		$_SESSION['xml_instances']['uploadedfile']['size'] = $_FILES['uploadedfile']['size'];
		$_SESSION['xml_instances']['uploadedfile']['tmp_name'] = $_FILES['uploadedfile']['tmp_name'];		
		copy($_FILES['uploadedfile']['tmp_name'], $path);
		$_SESSION['xml_instances']['uploadedfile']['path'] = $path;

	} elseif ($_FILES['uploadedfile']['error']>0) {
		switch ($_FILES['uploadedfile']['error']) {
			case 1: 
			case 2: echo "Failo įkėlimo klaida: per didelis failas"; exit;
			case 3: echo "Failo įkėlimo klaida: nepilnai įkeltas failas"; exit;
			default:
		}
	}
	
	header("Location: object_edit.php?xpath=" . $_REQUEST['xpath']);
?>
