<?php
	include("../include/config_user.php");
	include("../include/class.Classificator.php");

	$tpl = new FastTemplate("../templates/");
	$tpl -> define( array(
		main => "search_popup.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"class=\"active\"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	$field = $_GET["field"];
	$description = explode("lom/relation/resource/identifier/entry", $field);
	$db = dbc();
	$_SESSION["description"]= $description[1]+1;
	$classificator = new Classificator($db, 1);
	$classificator->createForm();
	$tpl -> assign(CLASSIFICATOR_OUTPUT,$classificator->form);
	$tpl -> assign(FIELD,$_REQUEST['field']);
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	
	exit;
?>
