<?php
	include("../include/config_user.php");
//	include("../include/lom.php");
	if(!isset($_GET['object_id'])) {
		header("Location: index.php");
	}

	$db = dbc();
	$db->Execute("delete from metadata where object=" . $_GET['object_id']);
	$db->Execute("delete from objects where id=" . $_GET['object_id']);
	
	unset($_SESSION['xml_instances']);
	unset($_SESSION['object_id']);
	
	if ($_SESSION['grp']=="a") {
		header("Location: ../admin/object_list.php");
	} else {
		header("Location: index.php");
	}
?>
