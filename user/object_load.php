<?php
	include("../include/config_user.php");
	if(!isset($_GET['object_id']) || strlen($_GET['language'])!=2) {
		header("Location: index.php");
		exit;
	}
	
	$language = $_GET['language']; // metadata language

	unset($_SESSION['xml_instances']); // prepare place for the new metadata
	
	$db = dbc();

        // save object info to the session
        $_SESSION['object_id']=$_GET['object_id'];
        $_SESSION['language']=$language;

	// if object has no foreign language translation use default
	if ($db->GetOne("select * from metadata where object=".$_GET['object_id'] . " and language='$language'")==0) { 
		$language = $CFG->default_metadata_language;
	} 

	$rs = $db->Execute("select * from metadata where object=".$_GET['object_id'] . " and language='$language'");
	while(!$rs->EOF) {
		$_SESSION['xml_instances'][$rs->fields['xpath']] = array (
					'id' => $rs->fields['item'],
					'key' => $rs->fields['xpath'],
					'value' => $rs->fields['value'],
					'parent' => $rs->fields['parent']					
					);				
		$rs -> MoveNext();
	}
	$rs = $db->Execute("select * from objects where id=".$_GET['object_id']);
	if($rs->fields['file_size']>0) {
		$_SESSION['xml_instances']['uploadedfile']['name'] = $rs->fields['file_name'];
		$_SESSION['xml_instances']['uploadedfile']['type'] = $rs->fields['file_type'];
		$_SESSION['xml_instances']['uploadedfile']['size'] = $rs->fields['file_size'];
		$_SESSION['xml_instances']['uploadedfile']['path'] = $rs->fields['file_path'];
	}
	$_SESSION['xml_instances']['author']=$rs->fields['author'];

	header("Location: object_edit.php?xpath=lom/general");
?>
