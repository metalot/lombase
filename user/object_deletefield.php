<?php
	include("../include/config_user.php");
	
	$xpath = $_REQUEST['xpath'];
	preg_match("!^(\w+/\w+)!", $xpath, $matches);
	$xtab = $matches[1]; 
	
	if (strlen($_REQUEST['xpath'])==0) {
		header("Location: object_edit.php?xpath=$xtab");
		exit;
	}
	
	function deleteLomSubtree($xpath) {
		$id = $_SESSION['xml_instances'][$xpath]['id'];
		foreach($_SESSION['xml_instances'] as $key => $value) {
			// find all children
			if($_SESSION['xml_instances'][$key]['parent'] == $id) {
				deleteLomSubtree($key);
			}
		}
		unset($_SESSION['xml_instances'][$xpath]);
	}

	deleteLomSubtree($xpath);
	
	header("Location: object_edit.php?xpath=$xtab");

?>
