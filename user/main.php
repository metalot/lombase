<?php
	include("../include/config.php");

	if(!isset($_SESSION['id'])) {
		header("Location: index.php");
	}
	
	$tpl = new FastTemplate("../templates/");
	$tpl -> define( array(
		head => "header.html",
		main => "main.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(MAIN,"Pradžia");
	$tpl->assign(MAIN_LINK,"main.php");
	$tpl->assign(ACTIVE_MAIN,"class=\"active\"");
	$tpl->assign(ACTIVE_SEARCH,"");
	$tpl->assign(ACTIVE_HELP,"");
	$name = $_SESSION['name']; $surname = $_SESSION['surname']; $grp = $_SESSION['grp'];
	$tpl -> assign(USER,"$name $surname");
	$tpl -> define_dynamic("view", "main");
	$db = dbc();

	$rs = $db->Execute("select * from objects LIMIT 10 SORT BY modified");
	while(!$rs->EOF) {
		// Valid login
		$tpl -> assign(ID, 	$rs->fields['id']);
		$tpl -> assign(TITLE_SHORT, $rs->fields['titel_kurz']);
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
