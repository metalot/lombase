<?php
	include("../include/config_user.php");
	include("../include/class.Classificator.php");
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header.html",
		main => "object_zip_upload.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"class=\"active\"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
?>	