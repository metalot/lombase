<?php
	include("../include/config_user.php");

	if ($_SESSION['grp']=="a") {
		header("Location: ../admin/");
		exit;
	}

	unset($_SESSION['xml_instances']);
	unset($_SESSION['object_id']);
	unset($_SESSION['language']);

	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> define( array(
		head => "header.html",
		main => "entrance.html",
		footer => "footer.html"
	));
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(ACTIVE_1,"class=\"active\"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");

	$tpl -> define_dynamic("view", "main");
	$db = dbc();

	$rs = $db->Execute("select * from objects where author=" . $_SESSION['user_id']);
	while(!$rs->EOF) {
		$rs2 = $db->Execute("select * from metadata where object=".$rs->fields['id']." and language='"  .$CFG->default_metadata_language. "' and xpath LIKE 'lom/general/title%'");
		// Valid login
		$tpl -> assign(ID, 	$rs->fields['id']);
		$object_id = $rs->fields['id'];
			//count objects clicks
			$counter=0;
			$rs3 = $db->Execute("select * from object_view_counter where object_id=$object_id and REQUEST_URL='object_view.php'");
				while(!$rs3->EOF) {
					$counter++;
				$rs3 -> MoveNext();
					}
					
		$tpl ->	assign(COUNTER, $counter);
		$tpl -> assign(TITLE_SHORT, $rs2->fields['value']);
		$tpl -> parse(viewlist, ".view");
		$rs -> MoveNext();
	}
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>
