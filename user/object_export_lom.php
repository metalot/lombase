<?php
	include("../include/config_user.php");
	include("../include/xml_struct.php");
	if(!isset($_GET['object_id'])) {
		header("Location: index.php");
	}
	$object_id = $_GET['object_id'];
	unset($_SESSION['xml_instances']);
	
	$db = dbc();
	$lom_xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";

	// load form metadata
	$rs_xml = $db->Execute("select * from meta1");
	$xml_db = $rs_xml->GetArray();
	$xmlForm = new XmlInputForm($xml_db);
	$rs_xml->free();

	$modified = $db->GetOne("select modified from objects where id=$object_id");
	$lom_xml .=  "\n<!-- Object id: $object_id last updated: $modified -->\n";
	$_SESSION['xml_instances'] = array (
					'lom0' => array ('id' => 0, 'key' => 'lom0', 'value' => '', 'parent'=>0)
	);

	$rs2 = $db->Execute("select * from metadata where object=$object_id");
	while(!$rs2->EOF) {
		$_SESSION['xml_instances'][$rs2->fields['xpath']] = array (
					'id' => $rs2->fields['item'],
					'key' => $rs2->fields['xpath'],
					'value' => $rs2->fields['value'],
					'parent' => $rs2->fields['parent']					
					);				
		$rs2 -> MoveNext();
	}

	$xmlForm->initXmlInstancesFromSession("lom"); // initialize object from session
	$xmlForm->fileXml("lom"); // generate form
	// echo $xmlForm->form;
	$lom_xml .= "
<lom xmlns=\"http://www.imsglobal.org/xsd/imsmd_rootv1p2p1\">
	<metaMetadata>
		<contribute>
			<role>
				<source>LREv3.0</source>
				<value>provider</value>
			</role>
			<entity>begin:vcard\n\t\torg:Lithuania ministry of education;Information technology center\n\t\tversion:2.1\n\t\tend:vcard\n\t\t</entity>
			<date>
				<dateTime>$modified</dateTime>
			</date>
		</contribute>
	</metaMetadata>
$xmlForm->form
</lom>";

	header('Pragma: private');
	header('Cache-control: private, must-revalidate');
        header("Content-Type: text/plain\n\n");
        header("Content-Length: " . strlen($lom_xml));
        header('Content-Disposition: attachment; filename="' . $object_id . '.xml"');
	header("Content-Transfer-Encoding: binary\n");
        echo $lom_xml;
	exit;

?>
