<?php
	include("../include/config_user.php");
	include("../include/xml_struct.php");
	include("../include/class.Classificator.php");
	$db = dbc();
			
	// load form metadata
	$rs = $db->Execute("select * from meta1");
	$xml_db = $rs->GetArray();
	$xmlForm = new XmlInputForm($xml_db);
	$xmlForm->saveXmlInstancesIntoSession(); // save new data (coming via form post) into session
	
	
	if(strlen($_REQUEST['nexturl'])>0) {
	
	header("Location: " . $_REQUEST['nexturl']);}
	
	if($_REQUEST['xpath']=="file") {
		$form_template = "object_addfile.html";
		if(is_array($_SESSION['xml_instances']['uploadedfile'])) {
			$form = " Prisegtas failas: <a href='object_showfile.php'>" . $_SESSION['xml_instances']['uploadedfile']['name'] . "</a>";
		}
	} else {
		$xmlForm->initXmlInstancesFromSession("lom"); // initialize object from session
		$xmlForm->generateXmlForm($_REQUEST['xpath']); // generate form
		$form = $xmlForm->form;
	//	$xmlForm->debugToFile();
		$form_template = "object_add1.html";
	// var_dump($xmlForm->xml_instances);
	// $xmlForm->test();
	}
	$_SESSION["requested_xpath"]=$_REQUEST['xpath'];
	//if is not set type by get method, take it from database by object_id
	if(!isset($_GET["type"])&&(isset($_SESSION['object_id']))){
		$object_id=$_SESSION['object_id'];
		$rs = $db->Execute("select item from metadata where object=$object_id AND `xpath` LIKE '%lom/educational/learningresourcetype%'");
		if(strlen($type_id)>0){
		$rs = $db->Execute("select * from objects_type where type_id=$type_id");
		while(!$rs->EOF) {
		        $data_type = $rs->fields['type'];
		        $rs -> MoveNext();
		}
	}
															}			
	$tpl = new FastTemplate("../templates/");
	$tpl -> assign(USER, $_SESSION['name'] . " " . $_SESSION['surname']);
	$tpl -> assign(ID, $_SESSION['object_id']);
	$tpl -> define( array(
		head => "header_object_add.html",
		main => $form_template, //load right template
		footer => "footer.html"
	));
	//load files from database
	if(isset($_SESSION['object_id'])){
		$object_id = $_SESSION['object_id'];
		$rs = $db->Execute("select id,type,data,type_id from object_data where id=$object_id order by type");
		while(!$rs->EOF) {
			$link = "<a href='./../learning_objects/$object_id/files/" . $rs->fields['data'] . "'>".$rs->fields['data']."</a>";
			$tpl -> assign(DATA5, $link);
		        $rs -> MoveNext();
			}		
        }	
	
	
	
	$tpl->assign(TITLE,"LOMBASE " . $version);
	$tpl->assign(OBJECT_DATA,$_SESSION['object_id'] . ": [" . $_SESSION['language'] . "]");
	$tpl->assign(ACTIVE_1,"");
	$tpl->assign(ACTIVE_2,"");
	$tpl->assign(ACTIVE_3,"");
	$tpl->assign(ACTIVE_4,"");
	$tpl->assign(ACTIVE_5,"");
	$tpl->assign(ACTIVE_6,"");
	$tpl->assign(ACTIVE_7,"");
	$tpl->assign(ACTIVE_8,"");
	$tpl->assign(ACTIVE_9,"");
	$tpl->assign(ACTIVE_10,"");
	$tpl->assign(ACTIVE_11,"");
	$tpl->assign(XPATH,$_REQUEST['xpath']);
	
	if ($_SESSION['grp']=="a") {
		$tpl->assign(BACKLINK,"../admin/object_list.php?author=" . $_SESSION['xml_instances']['author']);
	} else {
		$tpl->assign(BACKLINK,"index.php");
	}
	
	$tpl->assign(NEXTURL,'');
	
	switch($_REQUEST['xpath']) {
		case "lom/general":		$tpl->assign(ACTIVE_2,"class=\"active\""); break;
		case "lom/lifecycle":		$tpl->assign(ACTIVE_3,"class=\"active\""); break;
		case "lom/meta-metadata":	$tpl->assign(ACTIVE_4,"class=\"active\""); break;
		case "lom/technical":		$tpl->assign(ACTIVE_5,"class=\"active\""); break;
//		case "lom/educational":		$tpl->assign(ACTIVE_6,"class=\"active\""); break;
		case "lom/rights":		$tpl->assign(ACTIVE_7,"class=\"active\""); break;
		case "lom/relation":		$tpl->assign(ACTIVE_8,"class=\"active\""); break;
//		case "lom/annotation":		$tpl->assign(ACTIVE_9,"class=\"active\""); break;
		case "lom/classification":	$tpl->assign(ACTIVE_10,"class=\"active\""); break;
		case "file":			$tpl->assign(ACTIVE_11,"class=\"active\"");
		break;
		default: 			$tpl->assign(ACTIVE_2,"class=\"active\""); break;
	}
	//load intoheader JS functions
	$i=1;
	do {
		$script = $script."\n".$_SESSION["script".$i];
	
		$i++;
		} while(isset($_SESSION["script".$i]));
	$i=1;
	$tpl->assign("SCRIPT1", $script);
	do {
		unset($_SESSION["script".$i]);		
		$i++;
		} while(isset($_SESSION["script".$i]));
	
	$tpl->assign(FORM, $form);
	
	if(isset($_SESSION["type_id"])){
	$tpl->assign(SELECTED.$_SESSION["type_id"], 'selected="selected"');}
	$tpl -> parse(HEAD, "head");
	$tpl -> parse(FOOTER, "footer");
	$tpl -> parse(MAIN, "main");
	$tpl -> FastPrint(MAIN);
	exit;
?>

